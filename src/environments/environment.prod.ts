export const environment = {
  production: true,
  apiUrl: 'https://pernix-central-api-staging.herokuapp.com',
  // apiUrl: 'https://pernix-central-api-test.herokuapp.com',

  // storage keys
  auth_token: 'token',
  employee_key: 'employee',
  nav_title_key: 'nav_title',

  // roles
  roles: [
    { name: 'Admin', tier: 0 },
    { name: 'Crafter', tier: 1 },
    { name: 'Apprentice', tier: 2 },
  ]
};
