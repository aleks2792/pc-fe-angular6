import { TestBed } from '@angular/core/testing';

import { BadgeResolverService } from './badge-resolver.service';

describe('BadgeResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BadgeResolverService = TestBed.get(BadgeResolverService);
    expect(service).toBeTruthy();
  });
});
