import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { BadgesComponent } from './badges.component';
import { BadgeListComponent } from './badge-list/badge-list.component';
import { BadgeDetailsComponent } from './badge-details/badge-details.component';
import { BadgeFormComponent } from './badge-form/badge-form.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    BadgesComponent,
    BadgeListComponent,
    BadgeDetailsComponent,
    BadgeFormComponent
  ],
  providers: [],
  bootstrap: [BadgesComponent]
})
export class BadgesModule { }
