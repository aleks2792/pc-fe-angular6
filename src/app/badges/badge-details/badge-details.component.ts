import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../services/permissions.service';

import { Badge } from '../../models/badge';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-badge-details',
  templateUrl: './badge-details.component.html',
  styleUrls: ['./badge-details.component.css']
})
export class BadgeDetailsComponent implements OnInit {
  private apiUrl: string;
  badge: Badge;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private permissionsService: PermissionsService
  ) {
    this.apiUrl = environment.apiUrl;
  }

  fixedImageMediumPath(badge: Badge) {
    let url: string;
    if (badge.medium && badge.medium.includes('data')) {
      url = badge.medium;
    } else if (this.apiUrl.includes('localhost')) {
      url = `${this.apiUrl}${badge.medium}`;
    } else {
      url = badge.medium;
    }
    return url;
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { badge: any }) => {
      this.badge = data.badge;
    })
  }
}
