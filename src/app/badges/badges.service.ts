import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';
import { fbind } from 'q';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class BadgesService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getBadges() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/badges`;

    return this.http.get(url, this.httpOptions)
  }

  getBadge(badgeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/badges/${badgeId}`;

    return this.http.get(url, this.httpOptions)
  }

  createBadge(badge: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/badges`;

    var fd = new FormData();

    fd.append(`badge[name]`, badge.name);
    fd.append(`badge[point_value]`, badge.point_value);
    fd.append(`badge[description]`, badge.description);
    fd.append(`badge[attachment]`, badge.attachment);

    return this.http.post(url, fd, this.httpOptions);
  }

  updateBadge(badge: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/badges/${badge.id}`;

    var fd = new FormData();

    fd.append(`badge[name]`, badge.name);
    fd.append(`badge[point_value]`, badge.point_value);
    fd.append(`badge[description]`, badge.description);
    if (badge.attachment != undefined) {
      fd.append(`badge[attachment]`, badge.attachment);
    }

    return this.http.put(url, fd, this.httpOptions);
  }

  addEmployeeBadges(badge: any, employees: any[]) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/badges/${badge.id}`;

    let payload = {
      badge: {
        employee_attributes: []
      }
    };

    employees.forEach((employee: any) => {
      let newEmployee = {
        employee_id: employee.id
      };
      payload.badge.employee_attributes.push(newEmployee);
    });

    return this.http.put(url, payload, this.httpOptions);
  }
}
