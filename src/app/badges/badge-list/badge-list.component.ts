import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BadgesService } from '../badges.service';
import { PermissionsService } from '../../services/permissions.service';

import { Badge } from '../../models/badge';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-badge-list',
  templateUrl: './badge-list.component.html',
  styleUrls: ['./badge-list.component.css']
})
export class BadgeListComponent implements OnInit {
  private apiUrl: string;
  badges: Badge[];

  constructor(
    private badgesService: BadgesService,
    private router: Router,
    private permissionsService: PermissionsService
  ) {
    this.apiUrl = environment.apiUrl;
  }

  fixedImageThumbPath(badge: Badge) {
    let url: string;
    if (badge.square && badge.square.includes('data')) {
      url = badge.square;
    } else if (this.apiUrl.includes('localhost')) {
      url = `${this.apiUrl}${badge.square}`;
    } else {
      url = badge.square;
    }
    return url;
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin()
  }

  ngOnInit() {
    this.badgesService.getBadges().subscribe((data: any) => {
      this.badges = data;
    }, console.error)
  }
}
