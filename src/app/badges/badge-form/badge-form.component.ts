import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BadgesService } from '../badges.service';
import { PermissionsService } from '../../services/permissions.service';
import { NotificationsService } from '../../services/notifications.service';

import { Badge } from '../../models/badge';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-badge-form',
  templateUrl: './badge-form.component.html',
  styleUrls: ['./badge-form.component.css']
})
export class BadgeFormComponent implements OnInit {
  private apiUrl: string;
  badge: Badge;
  isNew: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private badgesService: BadgesService,
    private permissionsService: PermissionsService,
    private notificationsService: NotificationsService
  ) {
    this.apiUrl = environment.apiUrl;
  }

  addBadge(badge: Badge) {
    this.badgesService.createBadge(badge).subscribe((data: any) => {
      if (data.name == badge.name) {
        this.notificationsService.showNotification('Badge created successfully', 'success');
        this.router.navigate(['badges', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  updateBadge(badge: Badge) {
    this.badgesService.updateBadge(badge).subscribe((data: any) => {
      if (data.name == badge.name) {
        this.notificationsService.showNotification('Badge created successfully', 'success');
        this.router.navigate(['badges', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'badges', 'list'];
    } else {
      path = ['../../../', 'badges', this.badge.id, 'details'];
    }

    this.router.navigate(path);
  }

  onSelectedFile(uploadEvent: any) {
    var reader = new FileReader();

    reader.onload = (onLoadEvent: any) => {
      this.badge.original = onLoadEvent.target.result;
    }
    
    reader.readAsDataURL(uploadEvent.target.files[0]);
    this.badge.attachment = <File>uploadEvent.target.files[0];
  }

  fixedImagePath(badge: Badge) {
    let url: string;
    if (badge.original && badge.original.includes('data')) {
      url = badge.original;
    } else if (this.apiUrl.includes('localhost')) {
      url = `${this.apiUrl}${badge.original}`;
    } else {
      url = badge.original;
    }
    return url;
  }

  ngOnInit() {
    if (this.router.url == '/badges/new') {
      this.isNew = true;
      this.badge = new Badge();
    } else {
      this.isNew = false;
      this.route.data.subscribe((data: { badge: any }) => {
        this.badge = data.badge;
      }, console.error)
    }
  }
}
