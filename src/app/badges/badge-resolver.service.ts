import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { BadgesService } from './badges.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BadgeResolverService {

  private badgeId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private badgesService: BadgesService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
      this.badgeId = +route.paramMap.get('badge_id');

      return this.badgesService.getBadge(this.badgeId).pipe(
        take(1),
        map(badge => {
          let response: any;
          if (badge) {
            response = badge;
          } else {
            this.router.navigate(['badges', 'list']);
            response = null;
          }
          return response;
        })
      )
    }
}
