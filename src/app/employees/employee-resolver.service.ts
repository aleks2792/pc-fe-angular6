import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { EmployeesService } from './employees.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const EMPLOYEE_KEY = environment.employee_key;

@Injectable()
export class EmployeeResolverService implements Resolve<any> {

  private employeeId: number;
  private savedEmployee: any;
  private storage: Storage;

  constructor(
    private employeesService: EmployeesService,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.employeeId = +route.paramMap.get('employee_id');
    this.savedEmployee = this.storage.getItem(EMPLOYEE_KEY);

    return this.employeesService.getEmployee(this.employeeId).pipe(
      take(1),
      map(employee => {
        let response: any;
        if (this.savedEmployee.id == this.employeeId) {
          response = this.savedEmployee;
        } else if (employee) {
          response = employee;
        } else {
          this.router.navigate(['employees', 'list']);
          response = null;
        }
        return response;
      })
    );
  }
}
