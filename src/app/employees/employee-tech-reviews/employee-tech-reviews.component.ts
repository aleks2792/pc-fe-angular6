import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TechReviewService } from '../../services/tech-review.service';
import { PermissionsService } from '../../services/permissions.service';

import { Employee } from '../../models/employee';
import { TechReview } from '../../models/tech-review';

@Component({
  selector: 'app-employee-tech-reviews',
  templateUrl: './employee-tech-reviews.component.html',
  styleUrls: ['./employee-tech-reviews.component.css']
})
export class EmployeeTechReviewsComponent implements OnInit {
  employee: Employee;
  techReviews: TechReview[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private techReviewService: TechReviewService,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.isCurrentEmployee(this.employee);
  }

  ngOnInit() {
    this.route.data.subscribe((data: { employee: Employee }) => {
      this.employee = data.employee;
    })

    this.techReviewService.getEmployeeReviews(this.employee.id).subscribe((data: any) => {
      this.techReviews = data;
    })
  }
}
