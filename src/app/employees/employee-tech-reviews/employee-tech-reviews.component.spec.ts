import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTechReviewsComponent } from './employee-tech-reviews.component';

describe('EmployeeTechReviewsComponent', () => {
  let component: EmployeeTechReviewsComponent;
  let fixture: ComponentFixture<EmployeeTechReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeTechReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeTechReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
