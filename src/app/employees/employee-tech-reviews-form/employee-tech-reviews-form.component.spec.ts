import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTechReviewsFormComponent } from './employee-tech-reviews-form.component';

describe('EmployeeTechReviewsFormComponent', () => {
  let component: EmployeeTechReviewsFormComponent;
  let fixture: ComponentFixture<EmployeeTechReviewsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeTechReviewsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeTechReviewsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
