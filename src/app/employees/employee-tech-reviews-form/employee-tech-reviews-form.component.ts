import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TechReviewService } from '../../services/tech-review.service';
import { TcpCompetenciesService } from '../../admin/tcp-competencies/tcp-competencies.service';
import { EvaluationPeriodsService } from '../../services/evaluation-periods.service';
import { NotificationsService } from '../../services/notifications.service';

import { Employee } from '../../models/employee';
import { TechReview, TcpAnswer, TcpCommentExpectation, TcpPlan } from '../../models/tech-review';
import { EvaluationPeriod } from '../../models/evaluation-period';
import { TcpCompetency } from '../../models/tcp-competency';

@Component({
  selector: 'app-employee-tech-reviews-form',
  templateUrl: './employee-tech-reviews-form.component.html',
  styleUrls: ['./employee-tech-reviews-form.component.css']
})
export class EmployeeTechReviewsFormComponent implements OnInit {
  employee: Employee;
  techReview: TechReview;
  isNew: boolean;
  evaluationPeriods: EvaluationPeriod[];
  tcpCompetencies: TcpCompetency[];
  tcpValidPositions: string[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private techReviewService: TechReviewService,
    private tcpCompetenciesService: TcpCompetenciesService,
    private evaluationPeriodsService: EvaluationPeriodsService,
    private notificationsService: NotificationsService
  ) {
    this.tcpValidPositions = [
      'Novice', 'Advance beginner', 'Competent', 'Proficient', 'Expert'
    ];
  }

  addReview() {
    this.techReviewService.createEmployeeReview(this.employee.id, this.techReview).subscribe((data: any) => {
      if (data.employee.id == this.employee.id) {
        this.notificationsService.showNotification('TCP review created successfully', 'success');
        this.router.navigate(['employees', this.employee.id, 'tech-reviews']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  updateReview() {
    this.techReviewService.updateEmployeeReview(this.employee.id, this.techReview).subscribe((data: any) => {
      if (data.employee.id == this.employee.id) {
        this.notificationsService.showNotification('TCP review updated successfully', 'success');
        this.router.navigate(['employees', this.employee.id, 'tech-reviews']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  ngOnInit() {
    if (this.router.url.includes('/tech-reviews/new')) {
      this.isNew = true;
      this.techReview = new TechReview();
      this.techReview.evaluation_period = new EvaluationPeriod();
      this.techReview.tcp_answers = new Array<TcpAnswer>();

      this.tcpCompetenciesService.getTcpCompetencies().subscribe((data: any) => {
        this.tcpCompetencies = data;
  
        this.tcpCompetencies.forEach((competency) => {
          let newAnswer = new TcpAnswer();
          newAnswer.tcp_plan = new TcpPlan();
          newAnswer.tcp_comment_expectation= new TcpCommentExpectation();
          newAnswer.competency = competency.text;
  
          this.techReview.tcp_answers.push(newAnswer);
        })
      })
    } else {
      this.isNew = false;
      this.route.data.subscribe((data: { tech_review: TechReview }) => {
        this.techReview = data.tech_review;
      })
    }

    this.route.data.subscribe((data: { employee: Employee }) => {
      this.employee = data.employee;
    });

    this.evaluationPeriodsService.getEvaluationPeriods().subscribe((data: any) => {
      this.evaluationPeriods = data;
    });
  }
}
