import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { NotificationsService } from '../../services/notifications.service';
import { EmployeeBadgesService } from '../../services/employee-badges.service';
import { PermissionsService } from '../../services/permissions.service';

import { AddBadgesDialogComponent } from '../../dialogs/add-badges-dialog/add-badges-dialog.component';

import { Employee } from '../../models/employee';
import { Badge } from '../../models/badge';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  private apiUrl: string;
  employee: Employee;
  employeeBadges: any[];
  residentIdMask: any;
  nationalIdMask: any;
  phoneMask: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private permissionsService: PermissionsService,
    private notificationsService: NotificationsService,
    private employeeBadgesService: EmployeeBadgesService,
    private dialog: MatDialog
  ) {
    this.apiUrl = environment.apiUrl;
    this.nationalIdMask = [/\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/];
    this.residentIdMask = [/\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, /\d/];
    this.phoneMask = [/\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/];
  }

  mask(): any {
    return {
      mask: (value: any) => {
        if (value.length <= 15) 
          return this.nationalIdMask;
        else
          return this.residentIdMask;
      },
      guide: false
    };      
  }

  ngOnInit() {
    this.route.data.subscribe((data: { employee: any }) => {
      this.employee = data.employee;
      this.employeeBadges = this.chunkArray(this.employee.employee_badges, 5);
    })
  }

  fixedImageSquarePath(badge: Badge) {
    let url: string;
    if (badge.medium && badge.medium.includes('data')) {
      url = badge.medium;
    } else if (this.apiUrl.includes('localhost')) {
      url = `${this.apiUrl}${badge.medium}`;
    } else {
      url = badge.medium;
    }
    return url;
  }

  firstImage() {
    let url: string;
    if (this.apiUrl.includes('localhost')) {
      url = `${this.apiUrl}${this.employee.images[0].original}`;
    } else {
      url = this.employee.images[0].original;
    }
    return url;
  }

  counter(cant_items: number) {
    return new Array(cant_items);
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.isCurrentEmployee(this.employee);
  }

  canAddBadge() {
    return this.permissionsService.currentEmployeeIsAdmin() || !this.permissionsService.isCurrentEmployee(this.employee);
  }

  private chunkArray(myArray: any, chunk_size: number){
    var results = [];
    
    while (myArray.length) {
        results.push(myArray.splice(0, chunk_size));
    }
    
    return results;
  }

  addBadges() {
    const dialogRef = this.dialog.open(AddBadgesDialogComponent, {
      width: '90%',
      data: {
        employee: this.employee,
        asignation: { employee_id: this.employee.id, badge_id: null },
        confirm: 'Confirm',
        cancel: 'Cancel'
      }
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result.update) {
        this.employeeBadgesService.addBadgesToEmployee(result.asignation).subscribe((data: any) => {
          if (data.badge.id == result.asignation.badge_id) {
            this.notificationsService.showNotification('Badge added!', 'success');
            this.router.navigate(['employees', 'list']);
          } else {
            this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          }
        })
      }
    })
  }
}
