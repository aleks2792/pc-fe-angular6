import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Sort } from '@angular/material/sort';

import { PermissionsService } from '../../services/permissions.service';
import { EmployeesService } from '../employees.service';
import { Employee } from '../../models/employee';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[];
  sortedData: Employee[];
  employeeFilter: Employee;
  scheduleOnly: boolean;

  constructor(
    private employeesService: EmployeesService,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  daySchedule(employee: Employee) {
    let schedule = '';
    const todayDate = new Date();
    const todayDay = todayDate.getDay();

    if (todayDay - 1 < 5) {
      const workDay = employee.schedule.work_days[todayDay - 1];

      workDay.periods.forEach((p, i) => {
        let period = `${p.start_time} to ${p.end_time}, `;
        // tslint:disable-next-line:triple-equals
        if (i == workDay.periods.length - 1) {
          period = period.replace(/,\s*$/, '');
        }
        schedule += period;
      });
    } else {
      schedule = 'Week end';
    }

    return schedule;
  }

  dayScheduleWFH(employee: Employee) {
    let wfh = false;
    const todayDate = new Date();
    const todayDay = todayDate.getDay();

    if (todayDay - 1 < 5) {
      const workDay = employee.schedule.work_days[todayDay - 1];

      wfh = workDay.wfh;
    } else {
      wfh = true;
    }

    return wfh;
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  sortData(sort: Sort) {
    const data = this.employees.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return this.compare(a.first_name, b.first_name, isAsc);
        case 'last_name': return this.compare(a.last_name, b.last_name, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  ngOnInit() {
    this.scheduleOnly = false;
    this.employeesService.getEmployees().subscribe((data: any) => {
      this.employees = data;
      this.sortedData = this.employees.slice();
    }, console.error);
    this.employeeFilter = new Employee();
    this.employeeFilter.first_name = '';
  }
}
