import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  createEmployee(employee: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees`;

    let payload = {
      employee: {
        first_name: employee.first_name,
        last_name: employee.last_name,
        birthdate: employee.birthdate,
        email: employee.email,
        has_car: employee.has_car,
        on_vacation: employee.on_vacation,
        can_be_mentor: employee.can_be_mentor,
        blog_url: employee.blog_url,
        computer_serial_number: employee.computer_serial_number,
        mac_address: employee.mac_address,
        hire_date: employee.hire_date,
        active: employee.active,
        phone_number: employee.phone_number,
        id_number: employee.id_number,
        role_id: employee.role.id
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  getEmployees() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees`;

    return this.http.get(url, this.httpOptions)
  }

  getScoreboardEmployees() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    const url = `${this.apiUrl}/v1/all-active-employees?scoreboard=true`;

    return this.http.get(url, this.httpOptions)
  }

  getActiveEmployees() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/all-active-employees`;

    return this.http.get(url, this.httpOptions)
  }

  getEmployee(employeeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}`;

    return this.http.get(url, this.httpOptions)
  }

  updateEmployee(employee: any, passwordChange: boolean) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employee.id}`;

    let payload = {
      employee: {
        first_name: employee.first_name,
        last_name: employee.last_name,
        birthdate: employee.birthdate,
        email: employee.email,
        has_car: employee.has_car,
        on_vacation: employee.on_vacation,
        can_be_mentor: employee.can_be_mentor,
        blog_url: employee.blog_url,
        computer_serial_number: employee.computer_serial_number,
        mac_address: employee.mac_address,
        hire_date: employee.hire_date,
        active: employee.active,
        phone_number: employee.phone_number,
        id_number: employee.id_number,
        role_id: employee.role.id
      }
    };

    if (passwordChange && employee.password != 'undefined' && employee.password_confirmation != undefined) {
      payload.employee['password'] = employee.password;
      payload.employee['password_confirmation'] = employee.password_confirmation;
    }

    return this.http.put(url, payload, this.httpOptions);
  }

  updateEmployeeProfileImages(employee: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employee.id}`;

    var fd = new FormData();

    employee.images.forEach((image: any, index: number) => {
      if (image.id && image._destroy) {
        fd.append(`employee[images_attributes][${index}][id]`, image.id);
        fd.append(`employee[images_attributes][${index}][_destroy]`, image._destroy);
      } else if (image.attachment) {
        fd.append(`employee[images_attributes][${index}][attachment]`, image.attachment, image.attachment.name);
      }
    })

    return this.http.put(url, fd, this.httpOptions);
  }

  updateEmployeeTcpSkills(employeeId: number, skills: any[]) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employeeId}/update_tcp_skills`;

    let payload = {
      employee: {
        tcp_skills_attributes: []
      }
    };

    skills.forEach((skill: any) => {
      let newTcpSkill = {
        id: skill.id,
        employee_id: employeeId,
        skill_id: skill.skill.id,
        level: skill.level,
        _destroy: skill._destroy
      };

      payload.employee.tcp_skills_attributes.push(newTcpSkill);
    })

    return this.http.put(url, payload, this.httpOptions);
  }
}
