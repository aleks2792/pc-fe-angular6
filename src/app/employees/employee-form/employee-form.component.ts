import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { EmployeesService } from '../employees.service';
import { ScheduleService } from '../../services/schedule.service';
import { NotificationsService } from '../../services/notifications.service';
import { SkillsService } from '../../skills/skills.service';
import { PermissionsService } from '../../services/permissions.service';
import { RolesService } from '../../services/roles.service';

import { Employee } from '../../models/employee';
import { Skill } from '../../models/skill';
import { TcpSkill } from '../../models/tcp-skill';
import { Period } from '../../models/period';
import { Image } from 'src/app/models/image';
import { Role } from '../../models/role';

import { environment } from '../../../environments/environment';

import { MAT_STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css'],
  providers: [{
    provide: MAT_STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class EmployeeFormComponent implements OnInit {
  private apiUrl: string;
  employee: Employee;
  skills: Skill[];
  tcpSkills: TcpSkill[];
  validRoles: Role[];
  validSkillLevels: any;
  isNew: boolean;
  passwordChange: boolean;
  residentIdMask: any;
  nationalIdMask: any;
  phoneMask: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeesService: EmployeesService,
    private scheduleService: ScheduleService,
    private skillsService: SkillsService,
    private permissionsService: PermissionsService,
    private rolesService: RolesService,
    private notificationsService: NotificationsService
  ) {
    this.apiUrl = environment.apiUrl;
    this.validSkillLevels = [
      { name: 'Novice', level: '1' },
      { name: 'Beginner', level: '2' },
      { name: 'Competent', level: '3' },
      { name: 'Proficient', level: '4' },
      { name: 'Expert', level: '5' }
    ];

    this.nationalIdMask = [/\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/];
    this.residentIdMask = [/\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, /\d/];
    this.phoneMask = [/\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/];
  }

  mask(): any {
    return {
      mask: (value: any) => {
        if (value.length <= 15) 
          return this.nationalIdMask;
        else
          return this.residentIdMask;
      },
      guide: false
    };      
  }

  isApprentice(employee: Employee) {
    return this.permissionsService.employeeIsApprentice(employee);
  }

  currentEmployeeIsAdmin() {
    return this.permissionsService.currentEmployeeIsAdmin()
  }

  compareRoles(r1: any, r2: any): boolean {
    if (r1 != undefined && r2 != undefined) {
      return r1.tier === r2.tier;
    }
  }

  filteredPeriods(dayIndex: number) {
    return this.employee.schedule.work_days[dayIndex].periods.filter((p: any) => p._destroy === false);
  }

  addEmployee(employee: any) {
    this.employeesService.createEmployee(employee).subscribe((data: any) => {
      if (data.message == 'Account created successfully') {
        this.notificationsService.showNotification('Employee created successfully', 'success');
        this.router.navigate(['employees', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  updateEmployee(employee: any, passwordChange: boolean) {
    this.employeesService.updateEmployee(employee, passwordChange).subscribe((data: any) => {
      if (data.id == this.employee.id) {
        this.employee = data;
        this.notificationsService.showNotification('Employee updated successfully', 'success');
        this.router.navigate(['employees', employee.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  addPeriod(day: any) {
    let newPeriod: Period = {
      start_time: '00:00', end_time: '00:00', _destroy: false
    }

    this.employee.schedule.work_days[day].periods.push(newPeriod);
  }

  removePeriod(day: any, period: any) {
    this.employee.schedule.work_days[day].periods[period]._destroy = true;
  }

  updateSchedule(employeeId: number, schedule: any) {

    this.scheduleService.updateSchedule(employeeId, schedule).subscribe((data: any) => {
      if (data.id == this.employee.id) {
        this.employee = data;
        this.notificationsService.showNotification('Schedule updated successfully', 'success');
        this.router.navigate(['employees', employeeId, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  addSkill(skill: Skill, skill_index: number) {
    let push = true;
    let message: string;
    let newTcpSkill = {
      level: '',
      skill_id: skill.id,
      skill: skill,
      _destroy: false
    };

    this.tcpSkills.forEach((tcpSkill: TcpSkill) => {
      if (tcpSkill.skill_id == skill.id) {
        push = false;
      }
    })

    if (push) {
      this.skills.splice(skill_index, 1);
      this.tcpSkills.push(newTcpSkill);
    } else {
      message = `This employee already has the "${skill.name}" skill`;
      this.notificationsService.showNotification(message, 'warning');
    }
  }

  removeSkill(skill: Skill) {
    skill._destroy = true;
  }

  filteredTcpSkills() {
    let result: TcpSkill[];

    if (this.tcpSkills) {
      result = this.tcpSkills.filter((s: any) => s._destroy == false || s._destroy == 'undefined');
    }

    return result;
  }

  updateSkills() {
    this.employeesService.updateEmployeeTcpSkills(this.employee.id, this.tcpSkills).subscribe((data: any) => {
      if (data.id == this.employee.id) {
        this.employee = data;
        this.notificationsService.showNotification('Skills updated successfully', 'success');
        this.router.navigate(['employees', this.employee.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'employees', 'list'];
    } else {
      path = ['../../../', 'employees', this.employee.id, 'details'];
    }

    this.router.navigate(path);
  }

  canEdit() {
    if (this.isNew) {
      return false;
    } else {
      return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.currentEmployeeIsCrafter();
    }
  }

  onSelectedFile(uploadEvent: any, index: number) {
    var reader = new FileReader();
    let image = this.employee.images[index];

    reader.onload = (onLoadEvent: any) => {
      image.original = onLoadEvent.target.result;
    }

    reader.readAsDataURL(uploadEvent.target.files[0]);
    image.attachment = <File>uploadEvent.target.files[0];
  }

  fixedImagePath(image: Image) {
    let url: string;
    if (image.original && image.original.includes('data')) {
      url = image.original;
    } else if (this.apiUrl.includes('localhost')) {
      url = `${this.apiUrl}${image.original}`;
    } else {
      url = image.original;
    }
    return url;
  }

  removeImage(index: number) {
    this.employee.images[index]._destroy = true;
  }

  addImage() {
    let newImage = new Image();
    this.employee.images.push(newImage);
  }

  updateEmployeeProfileImages(employee: any) {
    this.employeesService.updateEmployeeProfileImages(employee).subscribe((data: any) => {
      if (data.id == this.employee.id) {
        this.employee = data;
        this.notificationsService.showNotification('Profile images updated successfully', 'success');
        this.router.navigate(['employees', employee.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  ngOnInit() {
    this.skillsService.getSkills().subscribe((data: any) => {
      this.skills = data;
    }, console.error);
    this.rolesService.getRoles().subscribe((data: any) => {
      this.validRoles = data;
    }, console.error);

    if (this.router.url == '/employees/new') {
      this.isNew = true;
      this.employee = new Employee();
      this.tcpSkills = new Array<TcpSkill>();
    } else {
      this.isNew = false;
      this.route.data.subscribe((data: { employee: any }) => {
        this.employee = data.employee;
        this.tcpSkills = data.employee.tcp_skills;
        this.tcpSkills.forEach((s: any) => {
          s._destroy = false;
        })

        this.employee.schedule.work_days.forEach((day: any) => {
          day.periods.forEach((period: any) => {
            period._destroy = false;
          })
        });
      })
    }
  }
}
