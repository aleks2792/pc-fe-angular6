import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PerformanceReviewService } from '../../services/performance-review.service';
import { PermissionsService } from '../../services/permissions.service';

import { Employee } from '../../models/employee';
import { PerformanceReview } from '../../models/performance-review';

@Component({
  selector: 'app-employee-performance-reviews',
  templateUrl: './employee-performance-reviews.component.html',
  styleUrls: ['./employee-performance-reviews.component.css']
})
export class EmployeePerformanceReviewsComponent implements OnInit {
  employee: Employee;
  performanceReviews: PerformanceReview[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private performanceReviewService: PerformanceReviewService,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.isCurrentEmployee(this.employee);
  }

  ngOnInit() {
    this.route.data.subscribe((data: { employee: Employee }) => {
      this.employee = data.employee;
    })

    this.performanceReviewService.getEmployeeReviews(this.employee.id).subscribe((data: any) => {
      this.performanceReviews = data;
    }, console.error)
  }

}
