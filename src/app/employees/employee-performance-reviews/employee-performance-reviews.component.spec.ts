import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePerformanceReviewsComponent } from './employee-performance-reviews.component';

describe('EmployeePerformanceReviewsComponent', () => {
  let component: EmployeePerformanceReviewsComponent;
  let fixture: ComponentFixture<EmployeePerformanceReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePerformanceReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePerformanceReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
