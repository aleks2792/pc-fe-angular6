import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PerformanceReviewService } from '../../services/performance-review.service';
import { EvaluationPeriodsService } from '../../services/evaluation-periods.service';
import { NotificationsService } from '../../services/notifications.service';

import { Employee } from '../../models/employee';
import { PerformanceReview, PrAchievement, PrImprovement, PrGoal } from '../../models/performance-review';
import { EvaluationPeriod } from '../../models/evaluation-period';

@Component({
  selector: 'app-employee-performance-reviews-form',
  templateUrl: './employee-performance-reviews-form.component.html',
  styleUrls: ['./employee-performance-reviews-form.component.css']
})
export class EmployeePerformanceReviewsFormComponent implements OnInit {
  employee: Employee;
  performanceReview: PerformanceReview;
  isNew: boolean;
  evaluationPeriods: EvaluationPeriod[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private performanceReviewService: PerformanceReviewService,
    private evaluationPeriodsService: EvaluationPeriodsService,
    private notificationsService: NotificationsService
  ) { }

  addReview() {
    this.performanceReviewService.createEmployeeReview(this.employee.id, this.performanceReview).subscribe((data: any) => {
      // tslint:disable-next-line:triple-equals
      if (data.employee.id == this.employee.id) {
        this.notificationsService.showNotification('Performance review created successfully', 'success');
        this.router.navigate(['employees', this.employee.id, 'performance-reviews']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  updateReview() {
    this.performanceReviewService.updateEmployeeReview(this.employee.id, this.performanceReview).subscribe((data: any) => {
      // tslint:disable-next-line:triple-equals
      if (data.employee.id == this.employee.id) {
        this.notificationsService.showNotification('Performance review created successfully', 'success');
        this.router.navigate(['employees', this.employee.id, 'performance-reviews']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  addAchievement() {
    const newAchievement = new PrAchievement();
    this.performanceReview.pr_achievements.push(newAchievement);
  }

  removeAchievement(index: number) {
    if (this.performanceReview.pr_achievements[index].id) {
      this.performanceReview.pr_achievements[index]._destroy = true;
    } else {
      this.performanceReview.pr_achievements.splice(index, 1);
    }
  }

  addImprovement() {
    const newImprovement = new PrImprovement();
    this.performanceReview.pr_improvements.push(newImprovement);
  }

  removeImprovement(index: number) {
    if (this.performanceReview.pr_improvements[index].id) {
      this.performanceReview.pr_improvements[index]._destroy = true;
    } else {
      this.performanceReview.pr_improvements.splice(index, 1);
    }
  }

  addGoal() {
    const newGoal = new PrGoal();
    this.performanceReview.pr_goals.push(newGoal);
  }

  removeGoal(index: number) {
    if (this.performanceReview.pr_goals[index].id) {
      this.performanceReview.pr_goals[index]._destroy = true;
    } else {
      this.performanceReview.pr_goals.splice(index, 1);
    }
  }

  ngOnInit() {
    if (this.router.url.includes('/performance-reviews/new')) {
      this.isNew = true;
      this.performanceReview = new PerformanceReview();
      this.performanceReview.evaluation_period = new EvaluationPeriod();
      this.performanceReview.pr_achievements = new Array<PrAchievement>();
      this.performanceReview.pr_improvements = new Array<PrImprovement>();
      this.performanceReview.pr_goals = new Array<PrGoal>();
    } else {
      this.isNew = false;
      this.route.data.subscribe((data: { performance_review: PerformanceReview }) => {
        this.performanceReview = data.performance_review;
      });
    }

    this.route.data.subscribe((data: { employee: Employee }) => {
      this.employee = data.employee;
    });

    this.evaluationPeriodsService.getEvaluationPeriods().subscribe((data: any) => {
      this.evaluationPeriods = data;
    });
  }
}
