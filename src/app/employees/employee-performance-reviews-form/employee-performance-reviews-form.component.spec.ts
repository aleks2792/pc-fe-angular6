import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePerformanceReviewsFormComponent } from './employee-performance-reviews-form.component';

describe('EmployeePerformanceReviewsFormComponent', () => {
  let component: EmployeePerformanceReviewsFormComponent;
  let fixture: ComponentFixture<EmployeePerformanceReviewsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePerformanceReviewsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePerformanceReviewsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
