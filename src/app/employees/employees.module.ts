import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { EmployeesComponent } from './employees.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { EmployeePerformanceReviewsComponent } from './employee-performance-reviews/employee-performance-reviews.component';
import { EmployeePerformanceReviewsFormComponent } from './employee-performance-reviews-form/employee-performance-reviews-form.component';
import { EmployeeTechReviewsComponent } from './employee-tech-reviews/employee-tech-reviews.component';
import { EmployeeTechReviewsFormComponent } from './employee-tech-reviews-form/employee-tech-reviews-form.component';

import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [
    EmployeesComponent,
    EmployeeListComponent,
    EmployeeDetailsComponent,
    EmployeeFormComponent,
    EmployeePerformanceReviewsComponent,
    EmployeePerformanceReviewsFormComponent,
    EmployeeTechReviewsComponent,
    EmployeeTechReviewsFormComponent,
  ],
  providers: [],
  bootstrap: [EmployeesComponent]
})
export class EmployeesModule { }
