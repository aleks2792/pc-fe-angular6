import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';
import { PerformanceReview } from '../models/performance-review';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class PerformanceReviewService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getEmployeeReviews(employeeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}/performance_reviews`;

    return this.http.get(url, this.httpOptions);
  }

  getEmployeeReview(employeeId: number, reviewId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}/performance_reviews/${reviewId}`;

    return this.http.get(url, this.httpOptions);
  }

  createEmployeeReview(employeeId: number, review: PerformanceReview) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}/performance_reviews`;

    let payload = {
      performance_review: {
        employee_id: employeeId,
        evaluation_period_id: review.evaluation_period.id,
        career_goal: review.career_goal,
        raise: review.raise,
        pr_achievements_attributes: [],
        pr_improvements_attributes: [],
        pr_goals_attributes: []
      }
    };

    review.pr_achievements.forEach((achievement) => {
      let newAchievement = {
        name: achievement.name,
        _destroy: achievement._destroy
      };

      payload.performance_review.pr_achievements_attributes.push(newAchievement);
    });

    review.pr_improvements.forEach((improvement) => {
      let newImprovement = {
        area: improvement.area,
        plan: improvement.plan
      };

      payload.performance_review.pr_improvements_attributes.push(newImprovement);
    });

    review.pr_goals.forEach((goal) => {
      let newGoal = {
        name: goal.name,
        metric: goal.metric,
        reward: goal.reward,
        accomplished: goal.accomplished,
        _destroy: goal._destroy
      };

      payload.performance_review.pr_goals_attributes.push(newGoal);
    });

    return this.http.post(url, payload, this.httpOptions);
  }

  updateEmployeeReview(employeeId: number, review: PerformanceReview) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}/performance_reviews/${review.id}`;

    let payload = {
      performance_review: {
        employee_id: employeeId,
        evaluation_period_id: review.evaluation_period.id,
        career_goal: review.career_goal,
        raise: review.raise,
        pr_achievements_attributes: [],
        pr_improvements_attributes: [],
        pr_goals_attributes: []
      }
    };

    review.pr_achievements.forEach((achievement) => {
      let newAchievement = {
        id: achievement.id,
        name: achievement.name,
        _destroy: achievement._destroy
      };

      payload.performance_review.pr_achievements_attributes.push(newAchievement);
    });

    review.pr_improvements.forEach((improvement) => {
      let newImprovement = {
        id: improvement.id,
        area: improvement.area,
        plan: improvement.plan,
        _destroy: improvement._destroy
      };

      payload.performance_review.pr_improvements_attributes.push(newImprovement);
    });

    review.pr_goals.forEach((goal) => {
      let newGoal = {
        id: goal.id,
        name: goal.name,
        metric: goal.metric,
        reward: goal.reward,
        accomplished: goal.accomplished,
        _destroy: goal._destroy
      };

      payload.performance_review.pr_goals_attributes.push(newGoal);
    });

    return this.http.put(url, payload, this.httpOptions);
  }
}
