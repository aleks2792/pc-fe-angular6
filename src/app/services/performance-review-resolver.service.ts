import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { PerformanceReviewService } from '../services/performance-review.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable()
export class PerformanceReviewResolverService {

  private performanceReviewId: number;
  private employeeId: number;
  private storage: Storage;

  constructor(
    private performanceReviewService: PerformanceReviewService,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.performanceReviewId = +route.paramMap.get('performance_review_id');
    this.employeeId = +route.paramMap.get('employee_id');

    return this.performanceReviewService.getEmployeeReview(this.employeeId, this.performanceReviewId).pipe(
      take(1),
      map(review => {
        let response: any;
        if (review) {
          response = review;
        } else {
          this.router.navigate(['employees', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
