import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';
import { TechReview } from '../models/tech-review';
import { CoreEnvironment } from '@angular/compiler/src/compiler_facade_interface';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class TechReviewService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getEmployeeReviews(employeeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}/tcp_reviews`;

    return this.http.get(url, this.httpOptions);
  }

  getEmployeeReview(employeeId: number, reviewId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}/tcp_reviews/${reviewId}`;

    return this.http.get(url, this.httpOptions);
  }

  createEmployeeReview(employeeId: number, review: TechReview) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    let payload = {
      tcp_review: {
        employee_id: employeeId,
        evaluation_period_id: review.evaluation_period.id,
        tcp_answers_attributes: []
      }
    };

    review.tcp_answers.forEach((answer) => {
      let newAnswer = {
        competency: answer.competency,
        current_position: answer.current_position,
        tcp_comment_expectation_attributes: {
          text: answer.tcp_comment_expectation.text,
          _destroy: answer.tcp_comment_expectation._destroy
        },
        tcp_plan_attributes: {
          text: answer.tcp_plan.text,
          _destroy: answer.tcp_plan._destroy
        },
        _destroy: answer._destroy
      };

      payload.tcp_review.tcp_answers_attributes.push(newAnswer);
    });

    const url = `${this.apiUrl}/v1/employees/${employeeId}/tcp_reviews`;

    return this.http.post(url, payload, this.httpOptions);
  }

  updateEmployeeReview(employeeId: number, review: TechReview) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    let payload = {
      tcp_review: {
        employee_id: employeeId,
        evaluation_period_id: review.evaluation_period.id,
        tcp_answers_attributes: []
      }
    };

    review.tcp_answers.forEach((answer) => {
      let updatedAnswer = {
        id: answer.id,
        competency: answer.competency,
        current_position: answer.current_position,
        tcp_comment_expectation_attributes: {
          id: answer.tcp_comment_expectation.id,
          text: answer.tcp_comment_expectation.text,
          _destroy: answer.tcp_comment_expectation._destroy
        },
        tcp_plan_attributes: {
          id: answer.tcp_plan.id,
          text: answer.tcp_plan.text,
          _destroy: answer.tcp_plan._destroy
        },
        _destroy: answer._destroy
      };

      payload.tcp_review.tcp_answers_attributes.push(updatedAnswer);
    });

    const url = `${this.apiUrl}/v1/employees/${employeeId}/tcp_reviews/${review.id}`;

    return this.http.put(url, payload, this.httpOptions);
  }
}
