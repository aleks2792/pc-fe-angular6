import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { TechReviewService } from '../services/tech-review.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TechReviewResolverService {

  private techReviewId: number;
  private employeeId: number;
  private storage: Storage;

  constructor(
    private techReviewService: TechReviewService,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.techReviewId = +route.paramMap.get('tech_review_id');
    this.employeeId = +route.paramMap.get('employee_id');

    return this.techReviewService.getEmployeeReview(this.employeeId, this.techReviewId).pipe(
      take(1),
      map(review => {
        let response: any;
        if (review) {
          response = review;
        } else {
          this.router.navigate(['employees', 'list']);
          response = null;
        }
        return response;
      })
    );
  }
}
