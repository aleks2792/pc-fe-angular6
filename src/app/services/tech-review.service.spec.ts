import { TestBed } from '@angular/core/testing';

import { TechReviewService } from './tech-review.service';

describe('TechReviewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TechReviewService = TestBed.get(TechReviewService);
    expect(service).toBeTruthy();
  });
});
