import { TestBed } from '@angular/core/testing';

import { PerformanceReviewResolverService } from './performance-review-resolver.service';

describe('PerformanceReviewResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PerformanceReviewResolverService = TestBed.get(PerformanceReviewResolverService);
    expect(service).toBeTruthy();
  });
});
