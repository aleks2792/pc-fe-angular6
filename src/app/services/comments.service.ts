import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

import { Comment } from '../models/comment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  createComment(comment: Comment) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/suggestions/${comment.suggestion_id}/comments`;

    let payload = {
      comment: {
        suggestion_id: comment.suggestion_id,
        employee_id: comment.employee_id,
        team_id: comment.team_id,
        text: comment.text,
        anonymus: comment.anonymus
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }
}
