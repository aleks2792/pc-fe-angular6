import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  updateSchedule(employeeId: number, schedule: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employeeId}/schedules/${schedule.id}`;

    let payload = {
      schedule: {
        id: schedule.id,
        work_days_attributes: []
      }
    }

    schedule.work_days.forEach((day: any) => {
      let work_day_attrs = {
        id: day.id,
        week_day: day.week_day,
        wfh: day.wfh,
        periods_attributes: []
      };

      day.periods.forEach((period: any) => {
        var period_attrs = {
          start_time: period.start_time,
          end_time: period.end_time,
          _destroy: period._destroy
        };

        if (period.id != 'undefined')
          period_attrs['id'] = period.id;

        work_day_attrs.periods_attributes.push(period_attrs);
      });

      payload.schedule.work_days_attributes.push(work_day_attrs);

    });

    return this.http.put(url, payload, this.httpOptions);
  }
}
