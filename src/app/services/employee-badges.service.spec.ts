import { TestBed } from '@angular/core/testing';

import { EmployeeBadgesService } from './employee-badges.service';

describe('EmployeeBadgesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmployeeBadgesService = TestBed.get(EmployeeBadgesService);
    expect(service).toBeTruthy();
  });
});
