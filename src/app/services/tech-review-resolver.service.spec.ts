import { TestBed } from '@angular/core/testing';

import { TechReviewResolverService } from './tech-review-resolver.service';

describe('TechReviewResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TechReviewResolverService = TestBed.get(TechReviewResolverService);
    expect(service).toBeTruthy();
  });
});
