import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StorageService } from '../../services/storage.service';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/employees/list', title: 'Employees', icon: 'dashboard', class: '' },
  // { path: '/skills/list', title: 'Skills', icon: 'build', class: '' },
  // { path: '/clients/list', title: 'Clients', icon: 'contacts', class: '' },
  // { path: '/projects/list', title: 'Projects', icon: 'work', class: '' },
  // { path: '/teams/list', title: 'Teams', icon: 'group', class: '' },
  { path: '/events/list', title: 'Events', icon: 'event', class: '' },
  { path: '/admin/notifications/list', title: 'Notifications', icon: 'notification_important', class: '' },
  { path: '/admin/tcp-competencies/list', title: 'TCP Competencies', icon: 'assignment', class: '' },
  { path: '/badges/list', title: 'Badges', icon: 'brightness_high', class: '' },
  { path: '/suggestions/list', title: 'Suggestion box', icon: 'feedback', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  private storage: Storage;

  constructor(
    private storageService: StorageService,
    private router: Router
  ) {
    this.storage = this.storageService.get();
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

  logOut() {
    this.storage.clear();
    this.router.navigate(['login']);
  }

}
