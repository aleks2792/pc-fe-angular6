import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StorageService } from '../../services/storage.service';

import { environment } from '../../../environments/environment';
import { Employee } from '../../models/employee';

const NAV_TITLE_KEY = environment.nav_title_key;
const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private storage: Storage;
  employee: Employee;

  constructor(
    private storageService: StorageService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.storage = this.storageService.get();
  }

  getTitle() {
    return this.storage.getItem(NAV_TITLE_KEY);
  }

  isLogin(): boolean {
    return this.router.url == '/login';
  }

  isScoreboard(): boolean {
    return this.router.url == '/scoreboard';
  }

  ngOnInit() {
    this.employee = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));
  }
}
