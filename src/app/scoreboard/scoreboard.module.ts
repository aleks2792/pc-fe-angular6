import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { ScoreboardComponent } from './scoreboard.component';

@NgModule({
  declarations: [
    ScoreboardComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule
  ],
  bootstrap: [
    ScoreboardComponent
  ]
})
export class ScoreboardModule { }
