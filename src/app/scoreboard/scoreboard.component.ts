import { Component, OnInit } from '@angular/core';

import { EmployeesService } from '../employees/employees.service';
import { Employee } from '../models/employee';

import { environment } from '../../environments/environment';
import { EmployeeBadge } from '../models/badge';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {
  employees: Employee[];
  firstPlace: Employee;
  secondPlace: Employee;
  thirdPlace: Employee;
  private apiUrl: string;

  constructor(
    private employeesService: EmployeesService
  ) {
    this.apiUrl = environment.apiUrl;
  }

  firstImage(employee: Employee) {
    let url: string;
    if (this.apiUrl.includes('localhost')) {
      url = `${this.apiUrl}${employee.images[0].original}`;
    } else {
      url = employee.images[0].original;
    }
    return url;
  }

  totalBadges(employee: Employee) {
    let total = 0;
    employee.employee_badges.forEach((badge: EmployeeBadge) => {
      total += badge.qty;
    })
    return total;
  }

  month() {
    let currentDate = new Date();
    return currentDate.toLocaleString('default', { month: 'long' });
  }

  ngOnInit() {
    this.employeesService.getScoreboardEmployees().subscribe((data: any) => {
      this.firstPlace = data[0];
      this.secondPlace = data[1];
      this.thirdPlace = data[2];

      this.employees = data;
    }, console.error);
  }
}
