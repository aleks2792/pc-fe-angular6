import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getProjects() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/projects`;

    return this.http.get(url, this.httpOptions)
  }

  getProject(projectId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/projects/${projectId}`;

    return this.http.get(url, this.httpOptions)
  }

  createProject(project: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/projects`;

    let payload = {
      project: {
        name: project.name,
        description: project.description,
        status: project.status,
        start_date: project.start_date,
        end_date: project.end_date,
        client_ids: [],
        team_id: project.team_id
      }
    };

    project.client_ids.forEach((client: any) => {
      payload.project.client_ids.push(client);
    });

    return this.http.post(url, payload, this.httpOptions);
  }

  updateProject(project: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/projects/${project.id}`;

    let payload = {
      project: {
        name: project.name,
        description: project.description,
        start_date: project.start_date,
        end_date: project.end_date,
        client_ids: [],
        team_id: project.team_id
      }
    };

    project.client_ids.forEach((client: any) => {
      payload.project.client_ids.push(client);
    });

    return this.http.put(url, payload, this.httpOptions);
  }
}
