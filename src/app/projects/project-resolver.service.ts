import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { ProjectsService } from './projects.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectResolverService {

  private projectId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private projectsService: ProjectsService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.projectId = +route.paramMap.get('project_id');

    return this.projectsService.getProject(this.projectId).pipe(
      take(1),
      map(project => {
        let response: any;
        if (project) {
          response = project;
        } else {
          this.router.navigate(['projects', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
