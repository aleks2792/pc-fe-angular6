import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectsService } from '../projects.service';
import { PermissionsService } from '../../services/permissions.service';
import { Project } from '../../models/project';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  projects: Project[];

  constructor(
    private projectsService: ProjectsService,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  status(project: any) {
    let status: string;

    if (project.status == 'on_track') {
      status = 'On track';
    } else if (project.status == 'ahead') {
      status = 'Ahead';
    } else if (project.status == 'behind') {
      status = 'Behind';
    } else if (project.status == 'finished') {
      status = 'Finished';
    } else {
      status = '';
    }

    return status;
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.currentEmployeeIsCrafter();
  }

  ngOnInit() {
    this.projectsService.getProjects().subscribe((data: any) => {
      this.projects = data;
    }, console.error);
  }
}
