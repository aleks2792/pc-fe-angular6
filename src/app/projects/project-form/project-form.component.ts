import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectsService } from '../projects.service';
import { ClientsService } from '../../clients/clients.service';
import { TeamsService } from '../../teams/teams.service';
import { NotificationsService } from '../../services/notifications.service';
import { Project } from '../../models/project';
import { Client } from '../../models/client';
import { Team } from '../../models/team';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {
  project: Project;
  clients: Client[];
  projectStatuses: any;
  teams: Team;
  isNew: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private projectsService: ProjectsService,
    private clientsService: ClientsService,
    private teamsService: TeamsService,
    private notificationsService: NotificationsService
  ) {
    this.projectStatuses = [
      { name: 'On track', symbol: 'on_track' },
      { name: 'Ahead', symbol: 'ahead' },
      { name: 'Behind', symbol: 'behind' },
      { name: 'Finished', symbol: 'finished' }
    ]
  }

  addProject(project: Project) {
    this.projectsService.createProject(project).subscribe((data: any) => {
      if (data.name == project.name) {
        this.notificationsService.showNotification('Project created successfully', 'success');
        this.router.navigate(['projects', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  updateProject(project: Project) {
    this.projectsService.updateProject(project).subscribe((data: any) => {
      if (data.name == project.name) {
        this.notificationsService.showNotification('Project updated successfully', 'success');
        this.router.navigate(['projects', project.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'projects', 'list'];
    } else {
      path = ['../../../', 'projects', this.project.id, 'details'];
    }

    this.router.navigate(path);
  }

  ngOnInit() {
    if (this.router.url == '/projects/new') {
      this.isNew = true;
      this.project = new Project();
    } else {
      this.route.data.subscribe((data: { project: any }) => {
        this.project = data.project;
        this.project.client_ids = [];
        this.project.clients.forEach((client: Client) => {
          this.project.client_ids.push(client.id);
        })
      });
      this.isNew = false;
    }

    this.clientsService.getClients().subscribe((data: any) => {
      this.clients = data;
    }, console.error);

    this.teamsService.getTeams().subscribe((data: any) => {
      this.teams = data;
    })
  }
}
