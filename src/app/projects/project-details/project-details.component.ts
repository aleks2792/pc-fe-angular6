import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../services/permissions.service';
import { Project } from '../../models/project';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  project: Project;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  status(project: any) {
    let status: string;

    if (project.status == 'on_track') {
      status = 'On track';
    } else if (project.status == 'ahead') {
      status = 'Ahead';
    } else if (project.status == 'behind') {
      status = 'Behind';
    } else if (project.status == 'finished') {
      status = 'Finished';
    } else {
      status = '';
    }

    return status;
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.currentEmployeeIsCrafter();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { project: any }) => {
      this.project = data.project;
    })
  }
}
