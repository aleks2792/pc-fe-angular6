import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TeamsService } from '../teams.service';
import { EmployeesService } from '../../employees/employees.service';
import { SkillsService } from '../../skills/skills.service';
import { NotificationsService } from '../../services/notifications.service';

import { Team } from '../../models/team';
import { Employee } from '../../models/employee';
import { TcpSkill } from '../../models/tcp-skill';
import { Skill } from '../../models/skill';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { MatChipInputEvent } from '@angular/material/chips';
import {
  MatAutocompleteSelectedEvent,
  MatAutocomplete
} from '@angular/material/autocomplete';

import {
  COMMA, ENTER, SPACE
} from '@angular/cdk/keycodes';

import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.css']
})
export class TeamFormComponent implements OnInit {
  team: Team;
  employees: Employee[];

  skillNames: string[];
  allSkillNames: string[];
  filteredSkillNames: Observable<string[]>;

  skillSearchCtrl = new FormControl();

  separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  addOnBlur = true;

  isNew: boolean;

  selectable = true;
  removable = true;

  @ViewChild('skillSearchInput') skillSearchInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private teamsService: TeamsService,
    private employeesService: EmployeesService,
    private skillsService: SkillsService,
    private notificationsService: NotificationsService
  ) {
    this.allSkillNames = [];
    this.skillNames = [];
    this.filteredSkillNames = this.skillSearchCtrl.valueChanges.pipe(
      startWith(null),
      map((skill: string | null) => skill ? this._filter(skill) : this.allSkillNames.slice())
    );
  }

  addTeam(team: Team) {
    this.teamsService.createTeam(team).subscribe((data: any) => {
      if (data.name == team.name) {
        this.notificationsService.showNotification('Team created successfully', 'success');
        this.router.navigate(['teams', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  updateTeam(team: Team) {
    this.teamsService.updateTeam(team).subscribe((data: any) => {
      if (data.name == team.name) {
        this.notificationsService.showNotification('Team updated successfully', 'success');
        this.router.navigate(['teams', team.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'teams', 'list'];
    } else {
      path = ['../../../', 'teams', this.team.id, 'details'];
    }

    this.router.navigate(path);
  }

  // chips for skill search methods

  addSkill(event: MatChipInputEvent) {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.skillNames.push(value.trim());
      }

      if (input) {
        input.value = '';
      }

      this.skillSearchCtrl.setValue(null);
    }
  }

  selectedSkill(event: MatAutocompleteSelectedEvent) {
    this.skillNames.push(event.option.viewValue);
    this.skillSearchInput.nativeElement.value = '';
    this.skillSearchCtrl.setValue(null);
  }

  removeSkill(skill: string) {
    const index = this.skillNames.indexOf(skill);

    if (index >= 0) {
      this.skillNames.splice(index, 1);
    }
  }

  private _filter(skill: string) {
    const filterValue = skill.toLowerCase();

    return this.allSkillNames.filter(skill => skill.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
    if (this.router.url == '/teams/new') {
      this.isNew = true;
      this.team = new Team();
    } else {
      this.route.data.subscribe((data: { team: any }) => {
        this.team = data.team;
        this.team.employee_ids = [];
        this.team.employees.forEach((employee: Employee) => {
          this.team.employee_ids.push(employee.id);
        })
      });
      this.isNew = false;
    }

    this.employeesService.getActiveEmployees().subscribe((data: any) => {
      this.employees = data;
      this.employees.forEach((e: Employee) => {
        e.skill_names = [];
        e.tcp_skills.forEach((s: TcpSkill) => {
          e.skill_names.push(s.skill.name);
        })
      })
    }, console.error);

    this.skillsService.getSkills().subscribe((data: any) => {
      data.forEach((skill: Skill) => {
        this.allSkillNames.push(skill.name);
      })
    }, console.error);
  }
}
