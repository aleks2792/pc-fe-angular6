import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { TeamsService } from './teams.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamResolverService {

  private teamId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private teamsService: TeamsService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.teamId = +route.paramMap.get('team_id');

    return this.teamsService.getTeam(this.teamId).pipe(
      take(1),
      map(team => {
        let response: any;
        if (team) {
          response = team;
        } else {
          this.router.navigate(['teams', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
