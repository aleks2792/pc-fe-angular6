import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { TeamsComponent } from './teams.component';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamFormComponent } from './team-form/team-form.component';

import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [
    TeamsComponent,
    TeamListComponent,
    TeamDetailsComponent,
    TeamFormComponent
  ],
  providers: [],
  bootstrap: [TeamsComponent]
})
export class TeamsModule { }
