import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Team } from '../../models/team';
import { TeamsService } from '../teams.service';
import { NotificationsService } from '../../services/notifications.service';
import { PermissionsService } from '../../services/permissions.service';

import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {
  team: Team;
  delete: boolean;

  constructor(
    private route: ActivatedRoute,
    private teamsService: TeamsService,
    private router: Router,
    private notificationsService: NotificationsService,
    private permissionsService: PermissionsService,
    private dialog: MatDialog
  ) { }

  deleteTeam(team: Team) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      data: {
        header: 'Are you sure?',
        message: 'Sure you want to delete this team? This action CANNOT be undone.',
        confirm: 'Confirm Delete',
        cancel: 'Cancel'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.delete = result.delete;
      if (this.delete) {
        this.teamsService.destroyTeam(team).subscribe((data: any) => {
          if (data == 'ok') {
            this.notificationsService.showNotification('Team deleted successfully', 'success');
            this.router.navigate(['teams', 'list']);
          } else {
            this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          }
        })
      }
    });
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.currentEmployeeIsCrafter();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { team: Team }) => {
      this.team = data.team;
    })
  }
}
