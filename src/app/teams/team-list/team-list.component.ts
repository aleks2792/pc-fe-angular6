import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TeamsService } from '../teams.service';
import { PermissionsService } from '../../services/permissions.service';
import { Team } from '../../models/team';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  teams: Team[];

  constructor(
    private teamsService: TeamsService,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.currentEmployeeIsCrafter();
  }

  ngOnInit() {
    this.teamsService.getTeams().subscribe((data: any) => {
      this.teams = data;
    }, console.error);
  }
}
