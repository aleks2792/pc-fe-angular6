import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getTeams() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/teams`;

    return this.http.get(url, this.httpOptions)
  }

  getTeam(teamId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/teams/${teamId}`;

    return this.http.get(url, this.httpOptions)
  }

  createTeam(team: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/teams`;

    let payload = {
      team: {
        name: team.name,
        project_id: team.project_id,
        employee_ids: team.employee_ids
      }
    }

    return this.http.post(url, payload, this.httpOptions);
  }

  updateTeam(team: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/teams/${team.id}`;

    let payload = {
      team: {
        name: team.name,
        project_id: team.project_id,
        employee_ids: team.employee_ids
      }
    }

    return this.http.put(url, payload, this.httpOptions);
  }

  destroyTeam(team: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/teams/${team.id}`;

    return this.http.delete(url, this.httpOptions);
  }
}
