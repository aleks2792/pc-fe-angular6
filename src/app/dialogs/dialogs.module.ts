import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AddParticipantsDialogComponent } from './add-participants-dialog/add-participants-dialog.component';
import { AddBadgesDialogComponent } from './add-badges-dialog/add-badges-dialog.component';
import { AddCommentsDialogComponent } from './add-comments-dialog/add-comments-dialog.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class DialogsModule { }
