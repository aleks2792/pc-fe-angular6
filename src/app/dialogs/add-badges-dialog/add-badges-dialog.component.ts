import { Component, Inject } from '@angular/core';

import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';

import { BadgesService } from '../../badges/badges.service';

import { Employee } from '../../models/employee';
import { Badge } from '../../models/badge';

export interface DialogData {
  employee: Employee,
  asignation: any,
  confirm: boolean,
  cancel: boolean
}

@Component({
  selector: 'app-add-badges-dialog',
  templateUrl: './add-badges-dialog.component.html',
  styleUrls: ['./add-badges-dialog.component.css']
})
export class AddBadgesDialogComponent {
  employee: Employee;
  badges: Badge[];
  asignation: any;

  constructor(
    public dialogRef: MatDialogRef<AddBadgesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private badgesService: BadgesService,
  ) {
    this.employee = data.employee;
    this.asignation = data.asignation;

    this.badgesService.getBadges().subscribe((data: any) => {
      this.badges = data;
    }, console.error)
  }

  confirm() {
    this.dialogRef.close({ asignation: this.asignation, update: true });
  }

  cancel() {
    this.dialogRef.close({ update: false });
  }
}
