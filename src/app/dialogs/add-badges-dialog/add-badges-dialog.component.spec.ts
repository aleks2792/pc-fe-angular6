import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBadgesDialogComponent } from './add-badges-dialog.component';

describe('AddBadgesDialogComponent', () => {
  let component: AddBadgesDialogComponent;
  let fixture: ComponentFixture<AddBadgesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBadgesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBadgesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
