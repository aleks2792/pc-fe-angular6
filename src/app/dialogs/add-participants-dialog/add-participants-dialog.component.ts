import { Component, Inject } from '@angular/core';

import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';

import { EmployeesService } from '../../employees/employees.service';
import { EventsService } from '../../events/events.service';
import { Employee } from '../../models/employee';
import { Participant } from '../../models/participant';


export interface DialogData {
  employees: Employee[];
  participants: Participant[];
  confirm: boolean;
  cancel: boolean;
}

@Component({
  selector: 'app-add-participants-dialog',
  templateUrl: './add-participants-dialog.component.html',
  styleUrls: ['./add-participants-dialog.component.css']
})
export class AddParticipantsDialogComponent {
  employees: Employee[];
  participants: Participant[];
  validTypes: any[];

  constructor(
    public dialogRef: MatDialogRef<AddParticipantsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,

  ) {
    this.employees = data.employees;
    this.participants = data.participants;

    this.validTypes = [
      { name: 'Owner', value: true },
      { name: 'Participant', value: false }
    ];
  }

  addParticipant(employee: Employee, employeeIndex: number) {
    let newParticipant = {
      employee: employee,
      is_owner: null,
      date: ''
    };

    this.data.participants.push(newParticipant);
  }

  confirm() {
    this.dialogRef.close({ participants: this.participants, update: true });
  }

  cancel() {
    this.dialogRef.close({ update: false });
  }
}
