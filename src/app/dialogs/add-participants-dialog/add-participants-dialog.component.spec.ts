import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddParticipantsDialogComponent } from './add-participants-dialog.component';

describe('AddParticipantsDialogComponent', () => {
  let component: AddParticipantsDialogComponent;
  let fixture: ComponentFixture<AddParticipantsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddParticipantsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddParticipantsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
