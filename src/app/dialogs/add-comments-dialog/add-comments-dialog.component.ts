import { Component, Inject } from '@angular/core';

import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';

import { Comment } from '../../models/comment';
import { Suggestion } from '../../models/suggestion';

export interface DialogData {
  suggestion: Suggestion,
  create: boolean,
  confirm: boolean,
  cancel: boolean
}

@Component({
  selector: 'app-add-comments-dialog',
  templateUrl: './add-comments-dialog.component.html',
  styleUrls: ['./add-comments-dialog.component.css']
})
export class AddCommentsDialogComponent {
  suggestion: Suggestion;
  comment: Comment;

  constructor(
    public dialogRef: MatDialogRef<AddCommentsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
    this.suggestion = data.suggestion;
    this.comment = new Comment();
    this.comment.suggestion_id = this.suggestion.id;
    this.comment.anonymus = true;
  }

  confirm() {
    this.dialogRef.close({ comment: this.comment, create: true });
  }

  cancel() {
    this.dialogRef.close({ create: false })
  }

}
