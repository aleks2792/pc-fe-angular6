import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../events.service';
import { NotificationsService } from '../../services/notifications.service';
import { Event } from '../../models/event';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css']
})
export class EventFormComponent implements OnInit {
  event: Event;
  frequencies: any[];
  isNew: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventsService: EventsService,
    private notificationsService: NotificationsService
  ) {
    this.frequencies = [
      { name: 'One time', value: 'once' },
      { name: 'Once a week', value: 'weekly' },
      { name: 'Once a month', value: 'monthly' }
    ]
  }

  addEvent(event: Event) {
    this.eventsService.createEvent(event).subscribe((data: any) => {
      if (data.name == event.name) {
        this.notificationsService.showNotification('Event created successfully', 'success');
        this.router.navigate(['events', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  updateEvent(event: Event) {
    this.eventsService.updateEvent(event).subscribe((data: any) => {
      if (data.name == event.name) {
        this.notificationsService.showNotification('Event updated successfully', 'success');
        this.router.navigate(['events', event.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'projects', 'list'];
    } else {
      path = ['../../../', 'events', this.event.id, 'details'];
    }

    this.router.navigate(path);
  }

  ngOnInit() {
    if (this.router.url == '/events/new') {
      this.isNew = true;
      this.event = new Event();
    } else {
      this.route.data.subscribe((data: { event: any }) => {
        this.event = data.event;
        this.isNew = false;
      })
    }
  }
}
