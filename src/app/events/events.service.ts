import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getEvents() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/activities`;

    return this.http.get(url, this.httpOptions)
  }

  getEvent(eventId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/activities/${eventId}`;

    return this.http.get(url, this.httpOptions)
  }

  createEvent(event: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/activities`;

    let payload = {
      activity: {
        name: event.name,
        points: event.points,
        frequency: event.frequency
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  updateEvent(event: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/activities/${event.id}`;

    let payload = {
      activity: {
        name: event.name,
        points: event.points,
        frequency: event.frequency
      }
    };

    return this.http.put(url, payload, this.httpOptions);
  }

  updateEventParticipants(event: any, participants: any[]) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/activities/${event.id}`;

    let payload = {
      activity: {
        participants_attributes: []
      }
    };

    participants.forEach((participant: any) => {
      let newParticipant = {
        employee_id: participant.employee.id,
        date: participant.date,
        is_owner: participant.is_owner
      };
      payload.activity.participants_attributes.push(newParticipant);
    });

    return this.http.put(url, payload, this.httpOptions);
  }
}
