import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Event } from '../../models/event';
import { Employee } from '../../models/employee';

import { MatDialog } from '@angular/material/dialog';
import { NotificationsService } from '../../services/notifications.service';

import { AddParticipantsDialogComponent } from '../../dialogs/add-participants-dialog/add-participants-dialog.component';
import { EmployeesService } from '../../employees/employees.service';
import { EventsService } from '../events.service';
import { PermissionsService } from '../../services/permissions.service';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {
  event: Event;
  employees: Employee;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private notificationsService: NotificationsService,
    private employeesService: EmployeesService,
    private eventsService: EventsService,
    private permissionsService: PermissionsService,
    private dialog: MatDialog
  ) { }

  addParticipants(event: Event) {
    const dialogRef = this.dialog.open(AddParticipantsDialogComponent, {
      width: '90%',
      data: {
        employees: this.employees,
        participants: [],
        confirm: 'Confirm',
        cancel: 'Cancel'
      }
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result.update) {
        this.eventsService.updateEventParticipants(this.event, result.participants).subscribe((data: any) => {
          if (data.name == event.name) {
            this.notificationsService.showNotification('Event updated successfully', 'success');
            this.router.navigate(['events', 'list']);
          } else {
            this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          }
        })
      }
    })
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.currentEmployeeIsCrafter();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { event: any }) => {
      this.event = data.event;
    })

    this.employeesService.getActiveEmployees().subscribe((data: any) => {
      this.employees = data;
    }, console.error)
  }
}
