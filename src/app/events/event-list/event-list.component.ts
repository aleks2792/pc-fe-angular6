import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventsService } from '../events.service';
import { PermissionsService } from '../../services/permissions.service';
import { Event } from '../../models/event';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {
  events: Event[];

  constructor(
    private eventsService: EventsService,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.currentEmployeeIsCrafter();
  }

  ngOnInit() {
    this.eventsService.getEvents().subscribe((data: any) => {
      this.events = data;
    }, console.error)
  }
}
