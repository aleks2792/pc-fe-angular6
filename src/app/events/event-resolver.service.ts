import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { EventsService } from './events.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventResolverService {

  private eventId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private eventsService: EventsService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.eventId = +route.paramMap.get('event_id');

    return this.eventsService.getEvent(this.eventId).pipe(
      take(1),
      map(event => {
        let response: any;
        if (event) {
          response = event;
        } else {
          this.router.navigate(['events', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
