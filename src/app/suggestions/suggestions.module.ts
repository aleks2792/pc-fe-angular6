import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { SuggestionsComponent } from './suggestions.component';
import { SuggestionListComponent } from './suggestion-list/suggestion-list.component';
import { SuggestionFormComponent } from './suggestion-form/suggestion-form.component';
import { SuggestionDetailsComponent } from './suggestion-details/suggestion-details.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    SuggestionsComponent,
    SuggestionListComponent,
    SuggestionFormComponent,
    SuggestionDetailsComponent
  ],
  providers: [],
  bootstrap: [SuggestionsComponent]
})
export class SuggestionsModule { }
