import { TestBed } from '@angular/core/testing';

import { SuggestionResolverService } from './suggestion-resolver.service';

describe('SuggestionResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuggestionResolverService = TestBed.get(SuggestionResolverService);
    expect(service).toBeTruthy();
  });
});
