import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SuggestionsService } from '../suggestions.service';
import { Suggestion } from '../../models/suggestion';

@Component({
  selector: 'app-suggestion-list',
  templateUrl: './suggestion-list.component.html',
  styleUrls: ['./suggestion-list.component.css']
})
export class SuggestionListComponent implements OnInit {
  suggestions: Suggestion[];

  constructor(
    private suggestionsService: SuggestionsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.suggestionsService.getSuggestions().subscribe((data: any) => {
      this.suggestions = data;
    }, console.error)
  }
}
