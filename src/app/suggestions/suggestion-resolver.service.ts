import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { SuggestionsService } from './suggestions.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SuggestionResolverService {

  private suggestionId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private suggestionsService: SuggestionsService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
      this.suggestionId = +route.paramMap.get('suggestion_id');

      return this.suggestionsService.getSuggestion(this.suggestionId).pipe(
        take(1),
        map(suggestion => {
          let response: any;
          if (suggestion) {
            response = suggestion;
          } else {
            this.router.navigate(['suggestions', 'list']);
            response = null;
          }
          return response;
        })
      )
    }
}
