import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SuggestionsService } from '../suggestions.service';
import { Suggestion } from '../../models/suggestion';

import { NotificationsService } from '../../services/notifications.service';

@Component({
  selector: 'app-suggestion-form',
  templateUrl: './suggestion-form.component.html',
  styleUrls: ['./suggestion-form.component.css']
})
export class SuggestionFormComponent implements OnInit {
  suggestion: Suggestion;

  froalaOptions: any = {
    placeholderText: 'Edit Your Content Here!',
    height: 300,
    fileUpload: false,
    imageUpload: false,
    videoUpload: false
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private suggestionsService: SuggestionsService,
    private notificationsService: NotificationsService
  ) { }

  addSuggestion(suggestion: Suggestion) {
    this.suggestionsService.createSuggestion(suggestion).subscribe((data: any) => {
      if (data.title == suggestion.title) {
        this.notificationsService.showNotification('Suggestion created successfully', 'success');
        this.router.navigate(['suggestions', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  goBack() {
    this.router.navigate(['../../../', 'suggestions', 'list']);
  }

  ngOnInit() {
    this.suggestion = new Suggestion();
  }
}
