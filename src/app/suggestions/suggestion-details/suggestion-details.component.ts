import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { Suggestion } from '../../models/suggestion';
import { Employee } from '../../models/employee';

import { SuggestionsService } from '../suggestions.service';
import { CommentsService } from '../../services/comments.service';
import { PermissionsService } from '../../services/permissions.service';
import { NotificationsService } from '../../services/notifications.service';
import { StorageService } from '../../services/storage.service';

import { AddCommentsDialogComponent } from '../../dialogs/add-comments-dialog/add-comments-dialog.component';

import { environment } from '../../../environments/environment';
const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-suggestion-details',
  templateUrl: './suggestion-details.component.html',
  styleUrls: ['./suggestion-details.component.css']
})
export class SuggestionDetailsComponent implements OnInit {
  private storage: Storage;
  employee: Employee;
  suggestion: Suggestion;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private suggestionsService: SuggestionsService,
    private commentsService: CommentsService,
    private permissionsService: PermissionsService,
    private notificationsService: NotificationsService,
    private storageService: StorageService,
    private dialog: MatDialog
  ) {
    this.storage = this.storageService.get();
  }

  addComment() {
    const dialogRef = this.dialog.open(AddCommentsDialogComponent, {
      width: '90%',
      data: {
        suggestion: this.suggestion,
        confirm: 'Confirm',
        cancel: 'Cancel'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.create) {
        result.comment.employee_id = !result.comment.anonymus ? this.employee.id : null;
        this.commentsService.createComment(result.comment).subscribe((data: any) => {
          if (data.text == result.comment.text) {
            this.suggestion.comments.push(data);
            this.notificationsService.showNotification('Comment added!', 'success');
          } else {
            this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          }
        });
      }
    });
  }

  markResolved() {
    this.suggestionsService.markSuggestionResolved(this.suggestion).subscribe((data: any) => {
      if (data.management_resolved) {
        this.notificationsService.showNotification('Suggestion marked as resolved!', 'success');
        this.suggestion = data;
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    });
  }

  markUnresolved() {
    this.suggestionsService.markSuggestionUnresolved(this.suggestion).subscribe((data: any) => {
      if (!data.management_resolved) {
        this.notificationsService.showNotification('Suggestion marked as unresolved!', 'success');
        this.suggestion = data;
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    });
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { suggestion: Suggestion }) => {
      this.suggestion = data.suggestion;
    });

    this.employee = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));
  }
}
