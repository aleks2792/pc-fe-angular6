import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../../components/components.module';

import { NotificationsComponent } from './notifications.component';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { NotificationFormComponent } from './notification-form/notification-form.component';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    NotificationsComponent,
    NotificationListComponent,
    NotificationFormComponent,
    NotificationDetailsComponent
  ],
  providers: [],
  bootstrap: [NotificationsComponent]
})
export class NotificationsModule { }
