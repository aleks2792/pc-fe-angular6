import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { Notification } from '../../../models/notification';

@Component({
  selector: 'app-notification-details',
  templateUrl: './notification-details.component.html',
  styleUrls: ['./notification-details.component.css']
})
export class NotificationDetailsComponent implements OnInit {
  notification: Notification;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  type(notification) {
    let type: string;

    if (notification.notification_type == 'birthday') {
      type = 'Birthday';
    } else if (notification.notification_type == 'one_time') {
      type = 'One Time';
    } else if (notification.notification_type == 'recurrent') {
      type = 'Recurrent';
    } else {
      type = '';
    }

    return type;
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { notification: any }) => {
      this.notification = data.notification;
    })
  }
}
