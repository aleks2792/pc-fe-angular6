import { Component, OnInit } from '@angular/core';
import { AdminNotificationsService } from '../admin-notifications.service';
import { PermissionsService } from '../../../services/permissions.service';
import { Notification } from '../../../models/notification';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {
  notifications: Notification[];

  constructor(
    private adminNotificationsService: AdminNotificationsService,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.adminNotificationsService.getNotifications().subscribe((data: any) => {
      this.notifications = data;
    }, console.error)
  }

}
