import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { AdminNotificationsService } from './admin-notifications.service';
import { StorageService } from '../../services/storage.service';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationResolverService {

  private notificationId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private adminNotificationsService: AdminNotificationsService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.notificationId = +route.paramMap.get('notification_id');

    return this.adminNotificationsService.getNotification(this.notificationId).pipe(
      take(1),
      map(notification => {
        let response: any;
        if (notification) {
          response = notification;
        } else {
          this.router.navigate(['notifications', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
