import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../../services/storage.service';

import { environment } from '../../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class AdminNotificationsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getNotifications() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/notifications`;

    return this.http.get(url, this.httpOptions);
  }

  getNotification(notificationId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/notifications/${notificationId}`;

    return this.http.get(url, this.httpOptions);
  }

  createNotification(notification: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/notifications`;

    let payload = {
      notification: {
        title: notification.title,
        notification_type: notification.notification_type,
        content: notification.content,
        date: notification.date,
        active: notification.active
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  updateNotification(notification: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/notifications/${notification.id}`;

    let payload = {
      notification: {
        title: notification.title,
        notification_type: notification.notification_type,
        content: notification.content,
        date: notification.date,
        active: notification.active
      }
    };

    return this.http.put(url, payload, this.httpOptions);
  }
}
