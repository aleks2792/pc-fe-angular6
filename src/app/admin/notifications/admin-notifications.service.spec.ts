import { TestBed } from '@angular/core/testing';

import { AdminNotificationsService } from './admin-notifications.service';

describe('AdminNotificationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminNotificationsService = TestBed.get(AdminNotificationsService);
    expect(service).toBeTruthy();
  });
});
