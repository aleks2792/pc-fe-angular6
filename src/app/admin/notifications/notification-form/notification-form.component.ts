import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminNotificationsService } from '../admin-notifications.service';
import { Notification } from '../../../models/notification';

import { NotificationsService } from '../../../services/notifications.service';

@Component({
  selector: 'app-notification-form',
  templateUrl: './notification-form.component.html',
  styleUrls: ['./notification-form.component.css']
})
export class NotificationFormComponent implements OnInit {
  notification: Notification;
  notificationTypes: any;
  isNew: boolean;
  froalaOptions: any = {
    placeholderText: 'Edit Your Content Here!',
    height: 300,
    fileUpload: false,
    imageUpload: false,
    videoUpload: false
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private adminNotificationsService: AdminNotificationsService,
    private notificationsService: NotificationsService
  ) {
    this.notificationTypes = [
      { name: 'Birthday', symbol: 'birthday' },
      { name: 'Anniversary', symbol: 'anniversary' },
      { name: 'Recurrent', symbol: 'recurrent' }
    ];
  }

  addNotification(notification: Notification) {
    this.adminNotificationsService.createNotification(notification).subscribe((data: any) => {
      if (data.title == notification.title) {
        this.notificationsService.showNotification('Notification created successfully', 'success');
        this.router.navigate(['admin', 'notifications', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  };

  updateNotification(notification: Notification) {
    this.adminNotificationsService.updateNotification(notification).subscribe((data: any) => {
      if (data.title == notification.title) {
        this.notificationsService.showNotification('Notification updated successfully', 'success');
        this.router.navigate(['admin', 'notifications', notification.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  };

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'admin', 'notifications', 'list'];
    } else {
      path = ['../../../', 'admin', 'notifications', this.notification.id, 'details'];
    }

    this.router.navigate(path);
  }

  isRecurrent(noti: Notification) {
    return noti.notification_type == 'recurrent';
  }

  ngOnInit() {
    if (this.router.url == '/admin/notifications/new') {
      this.isNew = true;
      this.notification = new Notification();
    } else {
      this.route.data.subscribe((data: { notification: any }) => {
        this.notification = data.notification;
      });
      this.isNew = false;
    }
  }
}
