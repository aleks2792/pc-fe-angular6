import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TcpCompetenciesComponent } from './tcp-competencies.component';

describe('TcpCompetenciesComponent', () => {
  let component: TcpCompetenciesComponent;
  let fixture: ComponentFixture<TcpCompetenciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TcpCompetenciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TcpCompetenciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
