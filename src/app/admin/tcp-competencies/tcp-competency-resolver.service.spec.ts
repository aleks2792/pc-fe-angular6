import { TestBed } from '@angular/core/testing';

import { TcpCompetencyResolverService } from './tcp-competency-resolver.service';

describe('TcpCompetencyResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TcpCompetencyResolverService = TestBed.get(TcpCompetencyResolverService);
    expect(service).toBeTruthy();
  });
});
