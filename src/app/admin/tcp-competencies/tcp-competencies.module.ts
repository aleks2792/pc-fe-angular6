import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../../components/components.module';

import { TcpCompetenciesComponent } from './tcp-competencies.component';
import { TcpCompetencyListComponent } from './tcp-competency-list/tcp-competency-list.component';
import { TcpCompetencyDetailsComponent } from './tcp-competency-details/tcp-competency-details.component';
import { TcpCompetencyFormComponent } from './tcp-competency-form/tcp-competency-form.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    TcpCompetenciesComponent,
    TcpCompetencyListComponent,
    TcpCompetencyDetailsComponent,
    TcpCompetencyFormComponent
  ],
  providers: [],
  bootstrap: [TcpCompetenciesComponent]
})
export class TcpCompetenciesModule { }
