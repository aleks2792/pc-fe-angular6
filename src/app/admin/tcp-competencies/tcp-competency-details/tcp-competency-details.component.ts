import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { TcpCompetency } from '../../../models/tcp-competency';

@Component({
  selector: 'app-tcp-competency-details',
  templateUrl: './tcp-competency-details.component.html',
  styleUrls: ['./tcp-competency-details.component.css']
})
export class TcpCompetencyDetailsComponent implements OnInit {
  tcpCompetency: TcpCompetency;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { competency: any }) => {
      this.tcpCompetency = data.competency;
    })
  }
}
