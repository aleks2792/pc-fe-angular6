import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TcpCompetencyDetailsComponent } from './tcp-competency-details.component';

describe('TcpCompetencyDetailsComponent', () => {
  let component: TcpCompetencyDetailsComponent;
  let fixture: ComponentFixture<TcpCompetencyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TcpCompetencyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TcpCompetencyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
