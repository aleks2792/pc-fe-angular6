import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TcpCompetenciesService } from '../tcp-competencies.service';
import { NotificationsService } from '../../../services/notifications.service';
import { TcpCompetency } from '../../../models/tcp-competency';

@Component({
  selector: 'app-tcp-competency-form',
  templateUrl: './tcp-competency-form.component.html',
  styleUrls: ['./tcp-competency-form.component.css']
})
export class TcpCompetencyFormComponent implements OnInit {
  tcpCompetency: TcpCompetency;
  isNew: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tcpCompetenciesService: TcpCompetenciesService,
    private notificationsService: NotificationsService
  ) { }

  addTcpCompetency(tcpCompetency: TcpCompetency) {
    this.tcpCompetenciesService.createTcpCompetency(tcpCompetency).subscribe((data: any) => {
      if (data.text == tcpCompetency.text) {
        this.notificationsService.showNotification('TCP Competency created successfully', 'success');
        this.router.navigate(['admin', 'tcp-competencies', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  updateTcpCompetency(tcpCompetency: TcpCompetency) {
    this.tcpCompetenciesService.updateTcpCompetency(tcpCompetency).subscribe((data: any) => {
      if (data.text == tcpCompetency.text) {
        this.notificationsService.showNotification('TCP Competency updated successfully', 'success');
        this.router.navigate(['admin', 'tcp-competencies', tcpCompetency.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error);
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'admin', 'tcp-competencies', 'list'];
    } else {
      path = ['../../../', 'admin', 'tcp-competencies', this.tcpCompetency.id, 'details'];
    }

    this.router.navigate(path);
  }

  ngOnInit() {
    if (this.router.url == '/admin/tcp-competencies/new') {
      this.isNew = true;
      this.tcpCompetency = new TcpCompetency();
    } else {
      this.route.data.subscribe((data: { competency: any }) => {
        this.tcpCompetency = data.competency;
      });
      this.isNew = false;
    }
  }
}
