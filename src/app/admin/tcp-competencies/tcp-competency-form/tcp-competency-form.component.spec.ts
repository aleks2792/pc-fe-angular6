import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TcpCompetencyFormComponent } from './tcp-competency-form.component';

describe('TcpCompetencyFormComponent', () => {
  let component: TcpCompetencyFormComponent;
  let fixture: ComponentFixture<TcpCompetencyFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TcpCompetencyFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TcpCompetencyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
