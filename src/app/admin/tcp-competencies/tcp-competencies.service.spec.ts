import { TestBed } from '@angular/core/testing';

import { TcpCompetenciesService } from './tcp-competencies.service';

describe('TcpCompetenciesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TcpCompetenciesService = TestBed.get(TcpCompetenciesService);
    expect(service).toBeTruthy();
  });
});
