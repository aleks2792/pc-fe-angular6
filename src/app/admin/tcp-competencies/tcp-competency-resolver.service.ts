import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { TcpCompetenciesService } from './tcp-competencies.service';
import { StorageService } from '../../services/storage.service';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TcpCompetencyResolverService {

  private competencyId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private tcpCompetenciesService: TcpCompetenciesService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.competencyId = +route.paramMap.get('competency_id');

    return this.tcpCompetenciesService.getTcpCompetency(this.competencyId).pipe(
      take(1),
      map(competency => {
        let response: any;
        if(competency) {
          response = competency;
        } else {
          this.router.navigate(['tcp-competencies', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
