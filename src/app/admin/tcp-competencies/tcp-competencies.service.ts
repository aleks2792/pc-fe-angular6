import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../../services/storage.service';

import { environment } from '../../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class TcpCompetenciesService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getTcpCompetencies() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/tcp_competencies`;

    return this.http.get(url, this.httpOptions)
  }

  getTcpCompetency(competencyId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/tcp_competencies/${competencyId}`;

    return this.http.get(url, this.httpOptions)
  }

  createTcpCompetency(competency: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    let payload = {
      tcp_competency: {
        text: competency.text
      }
    };

    const url = `${this.apiUrl}/v1/tcp_competencies`;

    return this.http.post(url, payload, this.httpOptions);
  }

  updateTcpCompetency(competency: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    let payload = {
      tcp_competency: {
        text: competency.text
      }
    };

    const url = `${this.apiUrl}/v1/tcp_competencies/${competency.id}`;

    return this.http.put(url, payload, this.httpOptions);
  }
}
