import { Component, OnInit } from '@angular/core';
import { TcpCompetenciesService } from '../tcp-competencies.service';
import { PermissionsService } from '../../../services/permissions.service';
import { TcpCompetency } from '../../../models/tcp-competency';

@Component({
  selector: 'app-tcp-competency-list',
  templateUrl: './tcp-competency-list.component.html',
  styleUrls: ['./tcp-competency-list.component.css']
})
export class TcpCompetencyListComponent implements OnInit {
  tcpCompetencies: TcpCompetency[];

  constructor(
    private tcpCompetenciesService: TcpCompetenciesService,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.tcpCompetenciesService.getTcpCompetencies().subscribe((data: any) => {
      this.tcpCompetencies = data;
    }, console.error)
  }
}
