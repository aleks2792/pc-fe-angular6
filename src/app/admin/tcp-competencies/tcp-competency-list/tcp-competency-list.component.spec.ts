import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TcpCompetencyListComponent } from './tcp-competency-list.component';

describe('TcpCompetencyListComponent', () => {
  let component: TcpCompetencyListComponent;
  let fixture: ComponentFixture<TcpCompetencyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TcpCompetencyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TcpCompetencyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
