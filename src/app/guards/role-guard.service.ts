import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PermissionsService } from '../services/permissions.service';
import { NotificationsService } from '../services/notifications.service';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {
  storage: Storage;

  constructor(
    private permissionsService: PermissionsService,
    private notificationsService: NotificationsService,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const expectedEmployeeId = +route.paramMap.get('employee_id');

    let response: boolean;
    let isAdminUser = this.permissionsService.currentEmployeeIsAdmin();
    let isCrafterUser = this.permissionsService.currentEmployeeIsCrafter();
    let isCurrentEmployee = this.permissionsService.isCurrentEmployeeId(expectedEmployeeId);

    if (state.url.includes('employees')) {
      if (state.url.includes('edit')) {
        response = isAdminUser || isCurrentEmployee;
      } else if (state.url.includes('performance-reviews')) {
        response = isAdminUser || isCurrentEmployee;
      } else if (state.url.includes('tech-reviews')) {
        response = isAdminUser || isCurrentEmployee;
      } else if (state.url.includes('new')) {
        response = isAdminUser;
      }
    } else if (state.url.includes('skills') && (state.url.includes('edit') || state.url.includes('new'))) {
      response = isAdminUser;
    } else if (state.url.includes('clients') && (state.url.includes('edit') || state.url.includes('new'))) {
      response = isAdminUser;
    } else if (state.url.includes('projects') && (state.url.includes('edit') || state.url.includes('new'))) {
      response = isAdminUser || isCrafterUser;
    } else if (state.url.includes('teams') && (state.url.includes('edit') || state.url.includes('new'))) {
      response = isAdminUser || isCrafterUser;
    } else if (state.url.includes('events') && (state.url.includes('edit') || state.url.includes('new'))) {
      response = isAdminUser || isCrafterUser;
    } else if (state.url.includes('notifications') && (state.url.includes('edit') || state.url.includes('new'))) {
      response = isAdminUser;
    } else if (state.url.includes('tcp-competencies')) {
      if (state.url.includes('edit') || state.url.includes('new')) {
        response = isAdminUser;
      } else {
        response = true;
      }
    } else if (state.url.includes('badges') && (state.url.includes('edit') || state.url.includes('new'))) {
      response = isAdminUser;
    }

    if (!response) {
      this.notificationsService.showNotification('Unauthorized!', 'danger');
      this.storage.clear();
      this.router.navigate(['login']);
      return response;
    } else {
      return response;
    }
  }
}
