import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';

import { LoginService } from '../login/login.service';
import { StorageService } from '../services/storage.service';
import { NotificationsService } from '../services/notifications.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  private storage: Storage;

  constructor(
    private loginService: LoginService,
    private storageService: StorageService,
    private router: Router,
    private notificationsService: NotificationsService
  ) {
    this.storage = this.storageService.get();
  }

  canActivate(): boolean {
    if (!this.loginService.isAuthenticated()) {
      this.notificationsService.showNotification('Unauthorized. You need to log in first!', 'danger');
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
