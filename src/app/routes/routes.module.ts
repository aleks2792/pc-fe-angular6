import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';

import { LoginComponent } from '../login/login.component';

import { EmployeesComponent } from '../employees/employees.component';
import { EmployeeListComponent } from '../employees/employee-list/employee-list.component';
import { EmployeeDetailsComponent } from '../employees/employee-details/employee-details.component';
import { EmployeeFormComponent } from '../employees/employee-form/employee-form.component';
import { EmployeePerformanceReviewsComponent } from '../employees/employee-performance-reviews/employee-performance-reviews.component';
import { EmployeePerformanceReviewsFormComponent } from '../employees/employee-performance-reviews-form/employee-performance-reviews-form.component';
import { EmployeeTechReviewsComponent } from '../employees/employee-tech-reviews/employee-tech-reviews.component';
import { EmployeeTechReviewsFormComponent } from '../employees/employee-tech-reviews-form/employee-tech-reviews-form.component';

import { EmployeeResolverService } from '../employees/employee-resolver.service';
import { PerformanceReviewResolverService } from '../services/performance-review-resolver.service';
import { TechReviewResolverService } from '../services/tech-review-resolver.service';

// import { SkillsComponent } from '../skills/skills.component';
// import { SkillListComponent } from '../skills/skill-list/skill-list.component';
// import { SkillDetailsComponent } from '../skills/skill-details/skill-details.component';
// import { SkillFormComponent } from '../skills/skill-form/skill-form.component';
// import { SkillResolverService } from '../skills/skill-resolver.service';

// import { ClientsComponent } from '../clients/clients.component';
// import { ClientListComponent } from '../clients/client-list/client-list.component';
// import { ClientDetailsComponent } from '../clients/client-details/client-details.component';
// import { ClientFormComponent } from '../clients/client-form/client-form.component';
// import { ClientResolverService } from '../clients/client-resolver.service';

// import { ProjectsComponent } from '../projects/projects.component';
// import { ProjectListComponent } from '../projects/project-list/project-list.component';
// import { ProjectDetailsComponent } from '../projects/project-details/project-details.component';
// import { ProjectFormComponent } from '../projects/project-form/project-form.component';
// import { ProjectResolverService } from '../projects/project-resolver.service';

// import { TeamsComponent } from '../teams/teams.component';
// import { TeamListComponent } from '../teams/team-list/team-list.component';
// import { TeamDetailsComponent } from '../teams/team-details/team-details.component';
// import { TeamFormComponent } from '../teams/team-form/team-form.component';
// import { TeamResolverService } from '../teams/team-resolver.service';

import { EventsComponent } from '../events/events.component';
import { EventListComponent } from '../events/event-list/event-list.component';
import { EventDetailsComponent } from '../events/event-details/event-details.component';
import { EventFormComponent } from '../events/event-form/event-form.component';
import { EventResolverService } from '../events/event-resolver.service';

import { ScoreboardComponent } from '../scoreboard/scoreboard.component';

import { NotificationsComponent } from '../admin/notifications/notifications.component';
import { NotificationListComponent } from '../admin/notifications/notification-list/notification-list.component';
import { NotificationDetailsComponent } from '../admin/notifications/notification-details/notification-details.component';
import { NotificationFormComponent } from '../admin/notifications/notification-form/notification-form.component';
import { NotificationResolverService } from '../admin/notifications/notification-resolver.service';

import { TcpCompetenciesComponent } from '../admin/tcp-competencies/tcp-competencies.component';
import { TcpCompetencyListComponent } from '../admin/tcp-competencies/tcp-competency-list/tcp-competency-list.component';
import { TcpCompetencyDetailsComponent } from '../admin/tcp-competencies/tcp-competency-details/tcp-competency-details.component';
import { TcpCompetencyFormComponent } from '../admin/tcp-competencies/tcp-competency-form/tcp-competency-form.component';
import { TcpCompetencyResolverService } from '../admin/tcp-competencies/tcp-competency-resolver.service';

import { BadgesComponent } from '../badges/badges.component';
import { BadgeListComponent } from '../badges/badge-list/badge-list.component';
import { BadgeFormComponent } from '../badges/badge-form/badge-form.component';
import { BadgeDetailsComponent } from '../badges/badge-details/badge-details.component';
import { BadgeResolverService } from '../badges/badge-resolver.service';

import { SuggestionsComponent } from '../suggestions/suggestions.component';
import { SuggestionListComponent } from '../suggestions/suggestion-list/suggestion-list.component';
import { SuggestionFormComponent } from '../suggestions/suggestion-form/suggestion-form.component';
import { SuggestionDetailsComponent } from '../suggestions/suggestion-details/suggestion-details.component';
import { SuggestionResolverService } from '../suggestions/suggestion-resolver.service';

import {
  AuthGuardService as AuthGuard
} from '../guards/auth-guard.service';
import {
  RoleGuardService as RoleGuard
} from '../guards/role-guard.service';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'scoreboard', component: ScoreboardComponent },
  {
    path: 'employees',
    component: EmployeesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: EmployeeListComponent
      },
      {
        path: ':employee_id/details',
        component: EmployeeDetailsComponent,
        resolve: {
          employee: EmployeeResolverService
        }
      },
      { path: 'new', component: EmployeeFormComponent, canActivate: [RoleGuard] },
      {
        path: ':employee_id/edit', component: EmployeeFormComponent,
        resolve: {
          employee: EmployeeResolverService
        },
        canActivate: [RoleGuard]
      },
      {
        path: ':employee_id/performance-reviews',
        component: EmployeePerformanceReviewsComponent,
        canActivate: [RoleGuard],
        resolve: {
          employee: EmployeeResolverService
        }
      },
      {
        path: ':employee_id/performance-reviews/:performance_review_id/edit',
        component: EmployeePerformanceReviewsFormComponent,
        canActivate: [RoleGuard],
        resolve: {
          employee: EmployeeResolverService,
          performance_review: PerformanceReviewResolverService
        }
      },
      {
        path: ':employee_id/performance-reviews/new',
        component: EmployeePerformanceReviewsFormComponent,
        canActivate: [RoleGuard],
        resolve: {
          employee: EmployeeResolverService
        }
      },
      {
        path: ':employee_id/tech-reviews',
        component: EmployeeTechReviewsComponent,
        canActivate: [RoleGuard],
        resolve: {
          employee: EmployeeResolverService
        }
      },
      {
        path: ':employee_id/tech-reviews/:tech_review_id/edit',
        component: EmployeeTechReviewsFormComponent,
        canActivate: [RoleGuard],
        resolve: {
          employee: EmployeeResolverService,
          tech_review: TechReviewResolverService
        }
      },
      {
        path: ':employee_id/tech-reviews/new',
        component: EmployeeTechReviewsFormComponent,
        canActivate: [RoleGuard],
        resolve: {
          employee: EmployeeResolverService
        }
      }
    ]
  },
  // {
  //   path: 'skills',
  //   component: SkillsComponent,
  //   canActivate: [AuthGuard],
  //   children: [
  //     {
  //       path: 'list',
  //       component: SkillListComponent
  //     },
  //     {
  //       path: ':skill_id/details',
  //       component: SkillDetailsComponent,
  //       resolve: {
  //         skill: SkillResolverService
  //       }
  //     },
  //     { path: 'new', component: SkillFormComponent, canActivate: [RoleGuard] },
  //     {
  //       path: ':skill_id/edit',
  //       component: SkillFormComponent,
  //       resolve: {
  //         skill: SkillResolverService
  //       },
  //       canActivate: [RoleGuard]
  //     }
  //   ]
  // },
  // {
  //   path: 'clients',
  //   component: ClientsComponent,
  //   canActivate: [AuthGuard],
  //   children: [
  //     {
  //       path: 'list',
  //       component: ClientListComponent
  //     },
  //     {
  //       path: ':client_id/details',
  //       component: ClientDetailsComponent,
  //       resolve: {
  //         client: ClientResolverService
  //       }
  //     },
  //     { path: 'new', component: ClientFormComponent, canActivate: [RoleGuard] },
  //     {
  //       path: ':client_id/edit',
  //       component: ClientFormComponent,
  //       resolve: {
  //         client: ClientResolverService
  //       },
  //       canActivate: [RoleGuard]
  //     }
  //   ]
  // },
  // {
  //   path: 'projects',
  //   component: ProjectsComponent,
  //   canActivate: [AuthGuard],
  //   children: [
  //     {
  //       path: 'list',
  //       component: ProjectListComponent
  //     },
  //     {
  //       path: ':project_id/details',
  //       component: ProjectDetailsComponent,
  //       resolve: {
  //         project: ProjectResolverService
  //       }
  //     },
  //     { path: 'new', component: ProjectFormComponent, canActivate: [RoleGuard] },
  //     {
  //       path: ':project_id/edit',
  //       component: ProjectFormComponent,
  //       resolve: {
  //         project: ProjectResolverService
  //       },
  //       canActivate: [RoleGuard]
  //     }
  //   ]
  // },
  // {
  //   path: 'teams',
  //   component: TeamsComponent,
  //   canActivate: [AuthGuard],
  //   children: [
  //     {
  //       path: 'list',
  //       component: TeamListComponent
  //     },
  //     {
  //       path: ':team_id/details',
  //       component: TeamDetailsComponent,
  //       resolve: {
  //         team: TeamResolverService
  //       }
  //     },
  //     { path: 'new', component: TeamFormComponent, canActivate: [RoleGuard] },
  //     {
  //       path: ':team_id/edit',
  //       component: TeamFormComponent,
  //       resolve: {
  //         team: TeamResolverService
  //       },
  //       canActivate: [RoleGuard]
  //     }
  //   ]
  // },
  {
    path: 'events',
    component: EventsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: EventListComponent
      },
      {
        path: ':event_id/details',
        component: EventDetailsComponent,
        resolve: {
          event: EventResolverService
        }
      },
      { path: 'new', component: EventFormComponent, canActivate: [RoleGuard] },
      {
        path: ':event_id/edit',
        component: EventFormComponent,
        resolve: {
          event: EventResolverService
        },
        canActivate: [RoleGuard]
      }
    ]
  },
  {
    path: 'admin/notifications',
    component: NotificationsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: NotificationListComponent
      },
      {
        path: ':notification_id/details',
        component: NotificationDetailsComponent,
        resolve: {
          notification: NotificationResolverService
        }
      },
      { path: 'new', component: NotificationFormComponent, canActivate: [RoleGuard] },
      {
        path: ':notification_id/edit',
        component: NotificationFormComponent,
        resolve: {
          notification: NotificationResolverService
        },
        canActivate: [RoleGuard]
      }
    ]
  },
  {
    path: 'admin/tcp-competencies',
    component: TcpCompetenciesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: TcpCompetencyListComponent,
        canActivate: [RoleGuard]
      },
      {
        path: ':competency_id/details',
        component: TcpCompetencyDetailsComponent,
        resolve: {
          competency: TcpCompetencyResolverService
        },
        canActivate: [RoleGuard]
      },
      {
        path: ':competency_id/edit',
        component: TcpCompetencyFormComponent,
        resolve: {
          competency: TcpCompetencyResolverService
        },
        canActivate: [RoleGuard]
      },
      { path: 'new', component: TcpCompetencyFormComponent, canActivate: [RoleGuard] }
    ]
  },
  {
    path: 'badges',
    component: BadgesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: BadgeListComponent
      },
      {
        path: ':badge_id/details',
        component: BadgeDetailsComponent,
        resolve: {
          badge: BadgeResolverService
        }
      },
      { path: 'new', component: BadgeFormComponent, canActivate: [RoleGuard] },
      {
        path: ':badge_id/edit',
        component: BadgeFormComponent,
        resolve: {
          badge: BadgeResolverService
        },
        canActivate: [RoleGuard]
      }
    ]
  },
  {
    path: 'suggestions',
    component: SuggestionsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: SuggestionListComponent
      },
      {
        path: 'new',
        component: SuggestionFormComponent
      },
      {
        path: ':suggestion_id/details',
        component: SuggestionDetailsComponent,
        resolve: {
          suggestion: SuggestionResolverService
        }
      }
    ]
  }
];

export function getToken(): string {
  return '';
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    JwtModule.forRoot({
      config: {
        tokenGetter: getToken
      }
    })
  ],
  exports: [RouterModule],
  providers: [
    EmployeeResolverService,
    PerformanceReviewResolverService,
    JwtHelperService,
  ]
})
export class RoutesModule { }
