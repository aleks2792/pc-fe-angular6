import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getClients() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/clients`;

    return this.http.get(url, this.httpOptions)
  }

  getClient(clientId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/clients/${clientId}`;

    return this.http.get(url, this.httpOptions)
  }

  createClient(client: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/clients`;

    let payload = {
      client: {
        name: client.name,
        last_name: client.last_name,
        contact_email: client.contact_email,
        company_name: client.company_name,
        phone_number: client.phone_number,
        country: client.country
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  updateClient(client: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/clients/${client.id}`;

    let payload = {
      client: {
        name: client.name,
        last_name: client.last_name,
        contact_email: client.contact_email,
        company_name: client.company_name,
        phone_number: client.phone_number,
        country: client.country
      }
    };

    return this.http.put(url, payload, this.httpOptions);
  }
}
