import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { ClientsComponent } from './clients.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { ClientFormComponent } from './client-form/client-form.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    ClientsComponent,
    ClientListComponent,
    ClientDetailsComponent,
    ClientFormComponent
  ],
  providers: [],
  bootstrap: [ClientsComponent]
})
export class ClientsModule { }
