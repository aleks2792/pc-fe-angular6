import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientsService } from '../clients.service';
import { NotificationsService } from '../../services/notifications.service';
import { Client } from '../../models/client';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {
  client: Client;
  isNew: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private clientsService: ClientsService,
    private notificationsService: NotificationsService
  ) { }

  addClient(client: Client) {
    this.clientsService.createClient(client).subscribe((data: any) => {
      if (data.name == client.name) {
        this.notificationsService.showNotification('Client created successfully', 'success');
        this.router.navigate(['clients', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  updateClient(client: Client) {
    this.clientsService.updateClient(client).subscribe((data: any) => {
      if (data.name == client.name) {
        this.notificationsService.showNotification('Client updated successfully', 'success');
        this.router.navigate(['clients', client.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'clients', 'list'];
    } else {
      path = ['../../../', 'clients', this.client.id, 'details'];
    }

    this.router.navigate(path);
  }

  ngOnInit() {
    if (this.router.url == '/clients/new') {
      this.isNew = true;
      this.client = new Client();
    } else {
      this.route.data.subscribe((data: { client: Client }) => {
        this.client = data.client;
      });
      this.isNew = false;
    }
  }
}
