import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { ClientsService } from './clients.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientResolverService {

  private clientId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private clientsService: ClientsService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.clientId = +route.paramMap.get('client_id');

    return this.clientsService.getClient(this.clientId).pipe(
      take(1),
      map(client => {
        let response: any;
        if (client) {
          response = client;
        } else {
          this.router.navigate(['clients', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
