import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientsService } from '../clients.service';
import { PermissionsService } from '../../services/permissions.service';
import { Client } from '../../models/client';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  clients: Client[];

  constructor(
    private clientsService: ClientsService,
    private router: Router,
    private permissionsService: PermissionsService
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.clientsService.getClients().subscribe((data: any) => {
      this.clients = data;
    }, console.error);
  }
}
