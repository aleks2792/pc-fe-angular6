import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../models/employee';

@Pipe({
  name: 'employeeBySkill',
  pure: false
})
export class EmployeeBySkillPipe implements PipeTransform {

  transform(employees: Employee[], filter: string[]): any {
    if (!employees || !filter) {
      return employees;
    } else {
      let response = [];
      filter.forEach((skill: string) => {
        employees.forEach((employee: Employee) => {
          if (employee.skill_names.includes(skill) && response.indexOf(employee) == -1) {
            response.push(employee);
          }
        })
      })
      return response;
    }
  }

}
