import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'skillType'
})
export class SkillTypePipe implements PipeTransform {

  transform(tcp_skills: any[], skill_type: string): any {
    return tcp_skills.filter(tcp_skill => tcp_skill.skill.skill_type == skill_type);
  }

}
