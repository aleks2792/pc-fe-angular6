import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkillTypePipe } from './skill-type.pipe';
import { EmployeeBySkillPipe } from './employee-by-skill.pipe';

import { FilterPipeModule } from 'ngx-filter-pipe';

@NgModule({
  declarations: [
    SkillTypePipe,
    EmployeeBySkillPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SkillTypePipe,
    EmployeeBySkillPipe,
    FilterPipeModule
  ]
})
export class PipesModule { }
