import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SkillsService } from '../skills.service';
import { PermissionsService } from '../../services/permissions.service';
import { Skill } from '../../models/skill';

@Component({
  selector: 'app-skills-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.css']
})
export class SkillListComponent implements OnInit {
  softSkills: Skill[];
  techSkills: Skill[];

  constructor(
    private skillsService: SkillsService,
    private router: Router,
    private permissionsService: PermissionsService
  ) {
    this.softSkills = [];
    this.techSkills = [];
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  ngOnInit() {
    this.skillsService.getSkills().subscribe((data: any) => {
      data.forEach((skill: Skill) => {
        if (skill.skill_type == 'tech') {
          this.techSkills.push(skill);
        } else if (skill.skill_type == 'soft') {
          this.softSkills.push(skill);
        }
      })
    }, console.error);
  }

}
