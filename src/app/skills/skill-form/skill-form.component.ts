import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SkillsService } from '../skills.service';
import { NotificationsService } from '../../services/notifications.service';
import { Skill } from '../../models/skill';

@Component({
  selector: 'app-skill-form',
  templateUrl: './skill-form.component.html',
  styleUrls: ['./skill-form.component.css']
})
export class SkillFormComponent implements OnInit {
  skill: Skill;
  validTypes: any;
  isNew: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private skillsService: SkillsService,
    private notificationsService: NotificationsService
  ) {
    this.validTypes = [
      { name: 'Soft Skill', symbol: 'soft' },
      { name: 'Tech Skill', symbol: 'tech' }
    ]
  }

  addSkill(skill: Skill) {
    this.skillsService.createSkill(skill).subscribe((data: any) => {
      if (data.name == skill.name) {
        this.notificationsService.showNotification('Skill created successfully', 'success');
        this.router.navigate(['skills', 'list']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  updateSkill(skill: Skill) {
    this.skillsService.updateSkill(skill).subscribe((data: any) => {
      if (data.name == skill.name) {
        this.notificationsService.showNotification('Skill updated successfully', 'success');
        this.router.navigate(['skills', skill.id, 'details']);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    }, console.error)
  }

  goBack() {
    let path: any[];

    if (this.isNew) {
      path = ['../../../', 'skills', 'list'];
    } else {
      path = ['../../../', 'skills', this.skill.id, 'details'];
    }

    this.router.navigate(path);
  }

  ngOnInit() {
    if (this.router.url == '/skills/new') {
      this.isNew = true;
      this.skill = new Skill();
    } else {
      this.route.data.subscribe((data: { skill: any }) => {
        this.skill = data.skill;
      });
      this.isNew = false;
    }
  }

}
