import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { SkillsComponent } from './skills.component';
import { SkillListComponent } from './skill-list/skill-list.component';
import { SkillDetailsComponent } from './skill-details/skill-details.component';
import { SkillFormComponent } from './skill-form/skill-form.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    SkillsComponent,
    SkillListComponent,
    SkillDetailsComponent,
    SkillFormComponent
  ],
  providers: [],
  bootstrap: [SkillsComponent]
})
export class SkillsModule { }
