import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { SkillsService } from './skills.service';
import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SkillResolverService {

  private skillId: number;
  private storage: Storage;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private skillsService: SkillsService
  ) {
    this.storage = this.storageService.get();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):

    Observable<any> {
    this.skillId = +route.paramMap.get('skill_id');

    return this.skillsService.getSkill(this.skillId).pipe(
      take(1),
      map(skill => {
        let response: any;
        if (skill) {
          response = skill;
        } else {
          this.router.navigate(['skills', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
