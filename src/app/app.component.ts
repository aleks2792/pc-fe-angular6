import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pc-fe-angular6';

  constructor(private router: Router) { }

  display(): boolean {
    let display: boolean;
    if (this.isLogin() || this.isScoreboard()) {
      display = false;
    } else {
      display = true;
    }
    return display;
  }

  isLogin(): boolean {
    return this.router.url == '/login';
  }

  isScoreboard(): boolean {
    return this.router.url == '/scoreboard';
  }
}
