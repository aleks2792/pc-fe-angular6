import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import { StorageService } from '../services/storage.service';

import { environment } from '../../environments/environment';

const AUTH_KEY = environment.auth_token;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LoginService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private jwtHelper: JwtHelperService
  ) {
    this.storage = this.storageService.get();
  }

  login(credentials: any) {
    const url = `${this.apiUrl}/v1/auth/login`;
    return this.http.post(url, credentials);
  }

  public isAuthenticated(): boolean {
    const token = this.storage.getItem(AUTH_KEY);
    return !this.jwtHelper.isTokenExpired(token);
  }
}
