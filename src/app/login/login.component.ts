import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LoginService } from './login.service';
import { StorageService } from '../services/storage.service';
import { NotificationsService } from '../services/notifications.service';

import { environment } from '../../environments/environment';
import { Employee } from '../models/employee';

const AUTH_KEY = environment.auth_token;
const EMPLOYEE_KEY = environment.employee_key;
const NAV_TITLE_KEY = environment.nav_title_key;

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: any;
  private storage: Storage;
  private employee: Employee;

  constructor(
    private loginService: LoginService,
    private storageService: StorageService,
    private router: Router,
    private notificationsService: NotificationsService
  ) {
    this.storage = this.storageService.get();
    this.credentials = {
      email: '',
      password: ''
    }
  }

  onSubmit() {
    this.loginService.login(this.credentials).subscribe((data: any) => {
      this.storage.setItem(AUTH_KEY, data.auth_token);
      this.storage.setItem(EMPLOYEE_KEY, JSON.stringify(data.employee));

      this.employee = data.employee;

      this.storage.setItem(NAV_TITLE_KEY, 'Crafting the Future!');
      this.router.navigate(['employees', 'list']);
    }, (error: any) => {
      this.notificationsService.showNotification(`${error.error.message}`, 'danger');
    })
  }

  ngOnInit() {
  }

}
