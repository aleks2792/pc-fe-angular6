import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { RoutesModule } from './routes/routes.module';
import { ComponentsModule } from './components/components.module';

import { LoginModule } from './login/login.module';
import { EmployeesModule } from './employees/employees.module';
import { SkillsModule } from './skills/skills.module';
import { ProjectsModule } from './projects/projects.module';
import { TeamsModule } from './teams/teams.module';
import { ClientsModule } from './clients/clients.module';
import { EventsModule } from './events/events.module';
import { ScoreboardModule } from './scoreboard/scoreboard.module';
import { NotificationsModule } from './admin/notifications/notifications.module';
import { TcpCompetenciesModule } from './admin/tcp-competencies/tcp-competencies.module';
import { BadgesModule } from './badges/badges.module';
import { SuggestionsModule } from './suggestions/suggestions.module';

import { StorageService, SessionStorage } from './services/storage.service';
import { LoginService } from './login/login.service';

import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { AddParticipantsDialogComponent } from './dialogs/add-participants-dialog/add-participants-dialog.component';
import { AddBadgesDialogComponent } from './dialogs/add-badges-dialog/add-badges-dialog.component';
import { AddCommentsDialogComponent } from './dialogs/add-comments-dialog/add-comments-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmDialogComponent,
    AddParticipantsDialogComponent,
    AddBadgesDialogComponent,
    AddCommentsDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutesModule,
    HttpClientModule,
    ComponentsModule,
    LoginModule,
    EmployeesModule,
    ProjectsModule,
    SkillsModule,
    TeamsModule,
    ClientsModule,
    EventsModule,
    ScoreboardModule,
    NotificationsModule,
    TcpCompetenciesModule,
    BadgesModule,
    SuggestionsModule
  ],
  providers: [
    { provide: StorageService, useClass: SessionStorage },
    LoginService,
  ],
  entryComponents: [
    ConfirmDialogComponent,
    AddParticipantsDialogComponent,
    AddBadgesDialogComponent,
    AddCommentsDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
