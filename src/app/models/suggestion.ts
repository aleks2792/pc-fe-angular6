import { Comment } from './comment';

export class Suggestion {
  id?: number;
  title: string;
  description: string;
  management_resolved: boolean;
  author: string;
  comments: Comment[];
}