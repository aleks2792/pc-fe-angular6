import { Employee } from './employee';
import { EvaluationPeriod } from './evaluation-period';

export class PerformanceReview {
  id?: number;
  career_goal: string;
  raise: number;
  employee: Employee;
  evaluation_period: EvaluationPeriod;
  pr_achievements: PrAchievement[];
  pr_improvements: PrImprovement[];
  pr_goals: PrGoal[];
};

export class PrAchievement {
  id?: number;
  name: string;
  _destroy: boolean;
};

export class PrImprovement {
  id?: number;
  area: string;
  plan: string;
  _destroy: boolean;
};

export class PrGoal {
  id?: number;
  name: string;
  metric: string;
  reward: number;
  accomplished: boolean;
  _destroy: boolean;
};
