export class Comment {
  id?: number;
  text: string;
  author: string;
  employee_id?: number;
  team_id?: number;
  suggestion_id?:number;
  anonymus: boolean;
  created_at: Date;
}