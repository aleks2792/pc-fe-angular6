import { Client } from './client';
import { Team } from './team';
import { Comment } from './comment';

export class Project {
  id?: number;
  name: string;
  description: string;
  start_date: string;
  end_date: string;
  status: string;
  active: boolean;
  comments: Comment[];
  clients: Client[];
  team?: Team;
  client_ids: number[];
  team_id?: number;
}