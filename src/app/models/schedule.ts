import { Workday } from './workday';

export class Schedule {
  id?: number;
  total_work_hours: number;
  work_days: Workday[];
}
