export class Period {
  id?: number;
  start_time: string;
  end_time: string;
  _destroy?: boolean;
}
