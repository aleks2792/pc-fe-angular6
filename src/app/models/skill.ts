export class Skill {
  id?: number;
  name: string;
  skill_type: string;
  _destroy?: boolean;
}
