export class EmployeeBadge {
  id: number;
  qty: number;
  badge: Badge;
}

export class Badge {
  id: number;
  name: string;
  point_value: number;
  description: string;
  created_at: string;
  updated_at: string;
  original: string;
  thumb: string;
  square: string;
  medium: string;
  attachment: File;
}