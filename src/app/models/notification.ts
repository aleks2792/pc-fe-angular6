export class Notification {
  id?: number;
  date: Date;
  notification_type: string;
  title: string;
  content: string;
  active: boolean;
  attachment: File;
  _destroy: boolean;
  original: any;
  thumb: string;
  square: string;
  medium: string;
};
