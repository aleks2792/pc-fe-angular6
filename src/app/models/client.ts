import { Project } from './project';

export class Client {
  id?: number;
  name: string;
  last_name: string;
  contact_email: string;
  phone_number: string;
  company_name: string;
  country: string;
  projects: Project[];
}
