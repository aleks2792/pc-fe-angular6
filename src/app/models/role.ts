export class Role {
  id: number;
  name: string;
  tier: number;
}
