import { Participant } from "./participant";

export class Event {
  id?: number;
  name: string;
  frequency: string;
  points: number;
  participants: Participant[];
}