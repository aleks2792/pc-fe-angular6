import { Schedule } from './schedule';
import { TcpSkill } from './tcp-skill';
import { Image } from './image';
import { EmployeeBadge } from './badge';
import { Role } from './role';

export class Employee {
  id?: number;
  name: string;
  first_name: string;
  last_name: string;
  birthdate: string;
  email: string;
  password: string;
  password_confirmation: string;
  has_car: boolean;
  blog_url: string;
  computer_serial_number: string;
  active: boolean;
  mac_address?: any;
  hire_date: string;
  on_vacation: boolean;
  can_be_mentor: boolean;
  role: Role;
  total_score: number;
  schedule: Schedule;
  tcp_skills: TcpSkill[];
  images: Image[];
  skill_names?: string[];
  phone_number: string;
  id_number: string;
  employee_badges: EmployeeBadge[];
}
