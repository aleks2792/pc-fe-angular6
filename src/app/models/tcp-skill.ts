import { Skill } from './skill';

export class TcpSkill {
  id?: number;
  level: string;
  skill_id: number;
  skill: Skill;
  _destroy?: boolean;
}
