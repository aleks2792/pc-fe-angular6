import { Employee } from "./employee";

export class Participant {
  id?: number;
  date: string;
  employee: Employee;
  is_owner: boolean;
}