export class Image {
  id?: number;
  attachment: File;
  _destroy: boolean;
  original: any;
  thumb: string;
  square: string;
  medium: string;
}
