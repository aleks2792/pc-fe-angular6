import { Employee } from './employee';
import { Project } from './project';

export class Team {
  id?: number;
  name: string;
  employees: Employee[];
  project?: Project;
  employee_ids?: number[];
}
