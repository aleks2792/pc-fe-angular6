import { Employee } from './employee';
import { EvaluationPeriod } from './evaluation-period';

export class TechReview {
  id?: number;
  employee: Employee;
  evaluation_period: EvaluationPeriod;
  tcp_answers: TcpAnswer[];
};

export class TcpAnswer {
  id?: number;
  competency: string;
  current_position: string;
  tcp_comment_expectation: TcpCommentExpectation;
  tcp_plan: TcpPlan;
  _destroy: boolean = false;
};

export class TcpCommentExpectation {
  id?: number;
  text: string;
  _destroy: boolean = false;
};

export class TcpPlan {
  id?: number;
  text: string;
  _destroy: boolean = false;
};
